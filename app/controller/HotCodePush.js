/**
 * The following events can be triggered by CHCP ( see https://github.com/nordnet/cordova-hot-code-push )
 *
 * chcp_updateIsReadyToInstall           - send when new release was successfully loaded and ready to be installed.
 * chcp_updateLoadFailed                 - send when plugin couldn't load update from the server. Error details are
 *                                         attached to the event.
 * chcp_nothingToUpdate                  - send when we successfully loaded application config from the server, but there
 *                                         is nothing new is available.
 * chcp_updateInstalled                  - send when update was successfully installed.
 * chcp_updateInstallFailed              - send when update installation failed. Error details are attached to the event.
 * chcp_nothingToInstall                 - send when there is nothing to install. Probably, nothing was loaded before that.
 * chcp_assetsInstalledOnExternalStorage - send when plugin successfully copied web project files from bundle onto the
 *                                         external storage. Most likely you will use it for debug purpose only. Or even never.
 * chcp_assetsInstallationError          - send when plugin couldn't copy files from bundle onto the external storage. If
 *                                         this happens - plugin won't work. Can occur when there is not enough free space
 *                                         on the device. Error details are attached to the event.
 *
 * Error Codes:
 *   1 - installation request was sent to the plugin, but there is nothing to install.
 *   2 - nothing new is available for download.
 *   -1 - failed to download new application config from the server. Either file doesn't exist or some internet connection problems.
 *   -2 - application's build version is too low for this update. New web release requires newer version of the app. User must update it through the store.
 *   -3 - failed to download new content manifest file from the server. Check that `chcp.manifest` file is placed in the root of your `content_url`, specified in the application config.
 *   -4 - failed to download updated/new files from the server. Check your `chcp.manifest` file: all listed files must be placed in the `content_url` from the application config. Also, check their hashes: they must match to the hashes in the `chcp.manifest`.
 *   -5 - failed to move downloaded files to the installation folder. Can occur when there is no free space on the device.
 *   -6 - update package is broken. Before installing anything plugin validates downloaded files once more by checking their hashes with the one that specified in the loaded `chcp.manifest` file. If they doesn't match or we are missing some file - this error is thrown.
 *   -7 - could not create backup before the installation. Can occur if device is out of free space. We need a backup in the case if something will go wrong during the installation so we could rollback.
 *   -8 - failed to copy new files to content directory. Can occur during the installation if there is not enough free space on device storage.
 *   -9 - failed to load current application config from the local storage. Can occur if user manually deleted plugin working directories from the external storage. If so - folders will be restored on the next launch.
 *   -10 - failed to load current manifest file from the local storage. Can occur if user manually deleted plugin working directories from the external storage. If so - folders will be restored on the next launch.
 *   -11 - failed to load new version of the application config from download folder (local storage). Can occur on installation process if user deletes plugin working directories from the external storage. If so - folders will be restored on the next launch.
 *   -12 - failed to load new version of the content manifest from download folder (local storage). Can occur on installation process if user deletes plugin working directories from the external storage. If so - folders will be restored on the next launch.
 *   -13 - failed to copy web project files from application bundle into external storage. Can occur if there is not enough free space on the users device. Action is performed on the first launch of the application. If it fails - plugin can't do it's job.
 */
Ext.define('the_app.controller.HotCodePush', {
    extend: Ext.app.Controller,
    config: {
        events: [
            'chcp_updateIsReadyToInstall',
            'chcp_updateLoadFailed',
            'chcp_nothingToUpdate',
            'chcp_updateInstalled',
            'chcp_updateInstallFailed',
            'chcp_nothingToInstall',
            'chcp_assetsInstalledOnExternalStorage',
            'chcp_assetsInstallationError',
        ],
        active: true,
        autoLoad: true, // roughing this in to allow for querying whether an update is available instead of automatically downloading ( which is how it currently works )
    },
    init: function() {
        if ( typeof chcp == 'undefined' ) {
            this.setActive( false );
            // There is no hot code push plugin loaded, bypass this
            return;
        }

        // This loop essentially just sets up a listener for each event where the listener simply fires the same event
        // but on this controller.  That allows other controllers to listen to events on the HotCodePush controller as
        // opposed to document.
        var me = this;
        Ext.each( this.getEvents(), function( eventName ) {
            var canonical = eventName.replace( /^chcp_/, ''), // string the chcp_ from the beginning of the event name
                listener = 'on' + canonical.charAt(0).toUpperCase() + canonical.slice(1)// i.e. onUpdateIsReadyToInstall
            ;

            document.addEventListener(
                eventName,
                // this[ listener ], // for some reason, this version ( which should work ) does not trigger on chcp_updateLoadFailed event, but
                function( event ){   // this version does.  Go figure.
                    me.fireEvent( eventName, event );
                },
                false
            );
            this.on( eventName, function( event ){
                this[ listener ]( event );
            }, this );
        }, this);
    },
    checkForHotPushUpdates: function( callback ) {
        if ( typeof callback == 'undefined' ) {
            callback = Ext.emptyFn
        }
        if ( !this.getActive() || typeof chcp == 'undefined' ) {
            this.fireEvent( 'chcp_nothingToUpdate' );
            callback();
            return;
        }
        chcp.fetchUpdate(callback);
    },
    onUpdateIsReadyToInstall: function( event ) {
        the_app.app.confirm({
            html: WP.__("This app has an update ready that requires it to restart.  Install now? "),
            hideOnMaskTap: false,
            handler: function() {
                chcp.installUpdate();
            }
        });
    },
    onUpdateLoadFailed: function( event ) {
        this.handleError( event )
    },
    onNothingToUpdate: function( event ) {
        // Nothing to update, do nothing
    },
    onUpdateInstalled: function( event ) {
        // Hurray, the update has been installed.  CHCP is about to trigger an app reload.  Nothing to do.
    },
    onUpdateInstallFailed: function( event ) {
        this.handleError( event )
    },
    onNothingToInstall: function( event ) {
        // Nothing to install, nothing to do
    },
    onAssetsInstalledOnExternalStorage: function( event ) {
        // Hurray, but really, who cares
    },
    onAssetsInstallationError: function( event ) {
        this.handleError( event );
    },
    handleError: function( event ) {
        var code = 0;
        if ( event.detail.error ) {
            code = event.detail.error.code;
        }

        switch( code ) {
        case -2:
            chcp.requestApplicationUpdate( WP.__( 'There is a new version of this app available in the app store.  Install now?' ) );
            break;
        case -5:
        case -7:
        case -8:
            the_app.app.alert({
                html: WP.__( "There is an update available for this app, but not enough space on your device to install it.  Please free some space and try again." )
            });
            break;
        default:
            // All other errors are benign and do not require any action
            break;
        }
    }
});