<?php 
    $the_app = the_app();
    $models = $the_app->models;
    $key = basename(get_query_var(APP_APP_VAR),'.js');
    $model = $models[$key];
    if ( get_query_var( 'NO_CONTENT_TYPE_HEADER' ) !== true ) {
        header('Content-type: text/javascript');
    }
    $model_keys = array_keys( $model );
    $last_model_key = end( $model_keys );
?>
Ext.define('the_app.model.<?php echo $key; ?>',
    {
        extend: 'Ext.data.ModelFaster',
        config: {
            <?php foreach ($model as $what => $details) : ?>
                <?php echo $what; ?>: <?php echo TheAppFactory::anti_escape(json_encode($details)); ?><?php echo ( $what == $last_model_key ) ? "\n" : ",\n"; ?>
            <?php endforeach; ?>
        }

    }
);