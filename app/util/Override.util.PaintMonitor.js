// Chrome 43 introduced some issues with ST firing the paint event, and the overflowchange event.  the following
// two overrides deal with that.  See discussion at http://trevorbrindle.com/chrome-43-broke-sencha/ and
// https://www.sencha.com/forum/announcement.php?f=92&a=58
Ext.define('Override.util.PaintMonitor', {
    override : 'Ext.util.PaintMonitor',

    constructor : function(config) {
        return new Ext.util.paintmonitor.CssAnimation(config);
    }
});
