<?php 
    // Please note: if you want to define a helper object, the object of that name will be
    // created.  For example, the file app/helper/WP.js instantiates a helper object called WP
    // Use the filter TheAppFactory_helpers to define helpers
    $the_app = the_app();
    $helpers = $the_app->helpers;
    $key = basename(get_query_var(APP_APP_VAR),'.js');
    $helper = $helpers[$key];
    $lambda = create_function( '$matches', 'return strtoupper( $matches[1] ); '); // turns an_attribute into anAttribute
    if ( get_query_var( 'NO_CONTENT_TYPE_HEADER' ) !== true ) {
        header('Content-type: text/javascript');
    }
    $helper_keys = array_keys( $helper );
    $last_helper_key = end( $helper_keys );
?>
Ext.define('the_app.helper.<?php echo $key; ?>', {
    config: {
        <?php foreach ($helper as $what => $details) : ?>
            <?php echo preg_replace_callback( '/_(.)/', $lambda, $what); ?>: <?php echo TheAppFactory::anti_escape(json_encode($details)); ?><?php echo ( $what == $last_helper_key ) ? "\n" : ",\n"; ?>
        <?php endforeach; ?>
    },

    singleton: true,
    alternateClassName: ['<?php echo $key; ?>'],

    constructor: function(config) {
        this.initConfig(config);
    }
});
