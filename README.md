WP App Factory
==============

The purpose of WP App Factory is to provide tools to be able to build and package iOS and Android apps from a WordPress
installation.  The apps are built using Sencha Touch as the underlying framework and Cordova as the packaging software 
to take the web application into a a native app.  

While WP App Factory does provide tools for simplifying the build process, really at its most basic, what it creates is
a Sencha Touch project file that you download to your local development box and then package ( using Xcode or Android
SDK ) for submission to the app store(s).

### Requirements

WP App Factory is going to be installed and activated on a web server that will host the app and its resources.  That 
web server must be a *nix box and be setup with a handful of command line tools as well as some environment variables.  
Its purpose is to be able to take an app definition in App Factory space and build a Sencha Touch project which you can 
then download and work on & package on your local dev box.  

##### Required Software on Server

The following tools must be installed on your server:

* [Sencha Cmd](https://www.sencha.com/products/sencha-cmd/) - this gives the command line program `sencha` which  
  creates the Sencha Project.  Tested with version 6.0.2.x.
* [Sencha Touch SDK](https://www.sencha.com/products/touch/) - the SDK for the underlying framework.  Expected to be 
  version 2.4.2.  
* [Cordova](http://cordova.apache.org/) - gives the `cordova` command and is the tool for packaging apps for the app
  store(s).  Tested with version 5.4.0.
* [NodeJS](https://nodejs.org/en/download/package-manager/) - gives the `node` command and is necessary for some of the 
  build steps.  Tested with version 0.10.24, which as it turns out is a bit of an older version.  The most current version
  should work just fine.
* [NPM](https://docs.npmjs.com/getting-started/installing-node) - gives the `npm` command.  This actually comes installed
  with Node, so you should have it already.  Tested with version 1.3.21, which is an older version.  The most current 
  version should work just fine.
* [Cordova Hot Code Push CLI](https://github.com/nordnet/cordova-hot-code-push-cli) - gives the `cordova-hcp` command.  
  This tool is used to archive a version of the app for update via the Hot Code Push plugin ( see more on that elsewhere
  in this readme ).
* [WP CLI](http://wp-cli.org/) - gives the `wp` command.  This allows the WordPress installation to be controlled via the
  command line
  
  
##### Additional Software Required on Development Box
  
On the development box, in addition to the above software, the following must be installed ( relevant to the platforms
for you wish you build ):

###### For iOS builds   
* [Xcode](https://developer.apple.com/xcode/) - For iOS only.  Version 7 is required.  
* [Cordova iOS](https://cordova.apache.org/announcements/2015/11/02/cordova-ios-3-9-2.html) - It is important that you 
  install version 3.9.2 ( not 4.x ) as the Cordova Hot Code Push plugin is not yet compatible with 4.x.  
     
###### For Android builds
* [Android SDK](http://developer.android.com/sdk/installing/index.html) - Testing was done with just the standalone SDK
  tools.  Install the latest versions.  Once this is installed, it gives the `android` command, which opens the Android
  SDK Manager.  From there, you'll install SDK build tools.  The following are necessary:
    * Android SDK Platform-tools - version 23.x
    * Android SDK Build-tools - version 23.x
    * Android 6.0 ( API 23 ) SDK Platform ( you don't need the documentation and samples )
    * Extras
        * Android Support Repository - version 25
        * Android Support Library - version 23.x
        * Google Play Services - version 29 or later
        * Google Repository - version 24 or later
* Cordova Android - tested with version 4.1.1 - `cordova platform update android@4.1.1`.  Probably the latest version ( 5.x )
  will work, but I've been testing with 4.1.1   
  
###### On Windows machines
  
So much of the process expects certain Linux commands to be available.  Please install [Git For Windows](https://git-for-windows.github.io/)
which gives you a bash shell on a windows box from which you'll invoke your `wp` commands.     
  
##### Required Environment Variables
  
WP App Factory expects the following environment
  
* Sencha Cmd must be on the PATH.  For example:
  
    export PATH="/path/to/Sencha/Cmd:$PATH"
    
* An environment variable called `SENCHA_SDK_242` must point to the location of the Sencha Touch SDK.  For example:

    export SENCHA_SDK_242="/usr/local/lib/touch-2.4.2/"
    
* Your `NODE_PATH` environment variable must point to the location of your `node_modules` directory.  For example:

    export NODE_PATH="/usr/lib/node_modules"

* In order to allow building of apps on the server from your local machine, you will need to setup a wp-cli.yml file
  in the root of your WordPress installation that will tell the `wp` command line tool about how to access the server.  
  Here is a sample file:
  
    url: yourlocal.installation.url
    path: /path/to/your/local/installation
    color: auto
    
    ssh:
    
      production:
        # The %pseudotty% placeholder gets replaced with -t or -T depending on whether you're piping output
        # The %cmd% placeholder is replaced with the originally-invoked WP-CLI command
        # Change xcdmobile.com to whatever hostname you have in your ~/.ssh/config file ( after setting up SSH key into the server )
        cmd: ssh %pseudotty% hostname %cmd%
    
        # Passed to WP-CLI on the remote server via --url
        url: remoteurl.com
    
        # We cd to this path on the remote server before running WP-CLI
        path: /path/on/server/to/public_html
    
        # WP-CLI over SSH will stop if one of these are provided
        disabled_commands:
          - db drop
          - db reset
          - plugin install
          - core multisite-convert
  
  A couple of things to note there.  The "production" is just a label and can be anything you want, but we will use that
  in further examples.  "hostname" in the `cmd` line is the name of the host you would use yourself in an `ssh hostname`
  command to ssh into the server.  You can either use a username@hostname format ( where you'll be prompted for a password
  every time you use the `wp ssh` command ) or setup an SSH key.  Doing that is beyond the scope of this README, but 
  Google is your friend.  It basically amounts to adding a public key to the .ssh/authorized_keys file on the server and 
  then adding an entry to your .ssh/config file something like this:
  
    Host myserver.com
    Hostname myserver.com
    IdentityFile ~/.ssh/id_rsa
    User username  

### WP CLI - The Command Line tool

The way WP App Factory is built is geared towards you being able to do what you need to do from the command line.  This
should greatly simplify your work flow.  The command to use is `wp app`.  Run `wp help app` to see the commands that are
available.

    NAME
    
      wp app
    
    DESCRIPTION
    
      Control various aspects of apps in WP App Factory.
    
    SYNOPSIS
    
      wp app <command>
    
    SUBCOMMANDS
    
      archive       Archives the app as it is right now for consumption by the Hot Code Push
      build         Build the app identified by the required slug.  The slug is found in the URL of
      clean         Clean out a build directory.
      config        Get or set configuration variables for the app.  Run `wp app config foo` to see a list of available variables that
      download      Download as a zip the built Sencha package for the app identified by the required slug.  If the app has not

For additional help and options for any of the subcommands, run `wp help app <subcommand>`.  

##### Invoking `wp app` remotely

Often you are going to be working on your local machine but need to run `wp app` commmands on a remote server.  The
`wp ssh app <subcommand> --host=<host>` variation is for exactly that purpose.  In order to set that up, you will need
to add a wp-cli.yml file to your local WordPress installation as described in the previous section.

### Typical Workflow

For this example, let's say we have an app with the slug `hello-world`.  This app exists in a WordPress installation
at http://yourdomain.com/apps/hello-world.  Let's further assume that you have customizations to the app in an add-on.
plugin that you created ( hooking into the various hooks of WP App Factory to get the app working how you need it to ).

It would be very wise for you to put your WordPress codebase ( including your plugin ) in a `git` repository.  Code
source control is definitely your friend.  If not, you'll need to be pushing changes to your live site via FTP.  For
these examples, I'm going to assume you are using the `git` approach.  

Create the app in your remote WordPress site with the following content.  This will be a single panel app.  

    [the_app]
        [app_item title="Home"]Here is some simple text[/app_item]
        
From your local development box, after you have successfully configured the `wp-cli.yml` file to allow `wp ssh app` to 
work, the first step is to build the app.  All `wp` commands need to be run from your local WordPress installation,
but you do not need to have the Hello World app defined in that local instance.  

*Note:*  If you just want to play around with things, you can work entirely on your local machine.  Anywhere below that
you see `wp ssh app <subcommand> --host=production`, simply substitute `wp app <subcommand>`.
        
    you$wordpress: wp ssh app build hello-world --host=production
    
This will trigger a build on the remote server that creates the Sencha Project with all of the required web resources
( Javascript, CSS, images ) for the app.  At this point, you can view your app by visiting http://yourdomain.com/apps/hello-world.
  
Now you'll want to download the Sencha project to do some local development on it.  Here I'm copying the zip file to 
`/path/to/other/workspace`.  Ideally that directory should be accessible via your local web server ( i.e. http://localhost/workspace )
  
    you$wordpress: wp ssh app download hello-world --host=production > /path/to/other/workspace/hello-world.zip
    you$wordpress: cd /path/to/other/workspace
    you$other-workspace: unzip hello-world.zip
    you$other-workspace: cd hello-world
    
At this point you can see the app by visiting, for example, http://localhost/workspace/hello-world.  The version of the
app you view there is a development version that has all of the necessary javascript files still sitting as separate files.
The directory structure will look like this:

    app
    app.js
    app.json
    bootstrap.js
    bootstrap.json
    build
    build.xml
    cordova
    index.html
    packages
    resources
    touch
    workspace.json

All of the app's Sencha Touch custom classes sit in the `app` directory and as you're developing, that's where you'll look.
     
Make some changes to some of the Sencha Touch classes in the `app` directory and reload the app.  Once you've completed
work on any files, you're going to need to upload them back up to your remote server and run the build/download commands
again. 
     
##### Packaging for iOS
     
When you're developing and want to get the app loaded into the iPhone simulator, you will need to invoke some sencha
commands to build and package the app.  

*Note:* The app, by default, has the Hot Code Push plugin enabled.  This is great for a production app because it will allow
web resources to be pushed to an installed app without requiring an app store update.  However, it can make development 
little tricky because all of the web resources get stored and loaded from the app instance.  For development, it's best 
to remove that plugin.  

    # Do not do this when you are preparing your app for the app store
    you$hello-world: cd cordova
    you$cordova: cordova plugin remove cordova-hot-code-push-plugin
    
Now, build the app.  For help with the `sencha` command, run `sencha help`.
    
    you$hello-world: sencha app build ios
    you$hello-world: open cordova/platforms/ios/HelloWorld.xcodeproj
    
This will build the app and open it up in Xcode, from where you'll be able to open it in the simulator or package it for
the app store. 

*Important:* further down you are going to read about triggering updates that require a new version to be downloaded 
from the app store.  In order for this piece to work, you need to tell WP App Factory the ios_identifier.  This is the
app id that is assigned when you create the app in iTune Connect.  You should do that well before you submit the app, 
so as soon as you have the id, please run:

    you$wordpress wp ssh app config hello-world --ios_identifier=id123456789

##### Packaging for Android

*Note* See the note in the iOS section about ( maybe ) removing the Hot Code Push Plugin

Building the app for Android is very similar.  I personally find that the Android emulators are brutally slow and I like
to install my development apk on my device.  So, at this point, I would plug my android device into my computer.

    you$hello-world: sencha app build android
    you$hello-world: cd cordova
    you$cordova: cordova run android
    
##### Archiving and Hot Code Pushes
    
WP App Factory uses the [Hot Code Push Plugin](https://github.com/nordnet/cordova-hot-code-push) to allow web resources
( javascript, css, images ) to be pushed to installed apps.  This is a great feature as it allows you to update your app
without requiring an app store update.  
    
When you are ready to package your app for production, you will want to archive the current version on the remote server.
    
    you$wordpress: wp ssh app build hello-world
    you$wordpress: wp ssh app archive hello-world --host=production
    
Anytime you've made changes to the app that you want to push to installed instances, you will run those two commands.  
WP App Factory takes care of the rest.
  
##### Hot Code Pushes That Require App Store Update
  
Sometimes, it might be necessary to force the user to install a new update from the app store.  This would be necessary
if you have added a new Cordova plugin, or you need to change the icon or startup image.  These resources all sit 
outside the web root and so therefore require an app store update.  Fortunately, WP App Factory makes that pretty easy.
All you have to do is increment the major version of the app.  To find out the current version, use the `wp app config`
command:
  
    you$wordpress: wp ssh app config hello-world --host=production
    
    ...
    
    # Version ( version )
    # The version number to build for the app stores.  Note, the first number ( the "major" version ) is used to determine 
    # if an app store update is required for a Hot Code Push.  You should increment the major version only if you are 
    # adding in new Cordova plugins.  Anything that touches only the web resources can and should stay in the same major 
    # version.
    1.0.1
    
    ... 
    
Here we see that the current version is 1.0.1.  To tell installed apps that there is an app store update available, just
change that version, then build & archive the app.  Note the --clean option on the build command, which tells the app to 
build from scratch, re-including Cordova plugins and icons/startup images.
      
    $you:wordpress: wp ssh app config hello-world --version=2.0.0 --host=production
    $you:wordpress: wp ssh app build hello-world --clean       
    $you:wordpress: wp ssh app download hello-world --host=production > /path/to/other/workspace/hello-world.zip
    $you:wordpress: cd /path/to/other/workspace && unzip hello-world.zip
    
Now package your app for the different stores and submit them.  For Android, it's available almost immediately, but
for iOS, it can take a little while.  When they are *both* available, then archive the new version, which will tell
Hot Code Push plugin to tell the apps that there is a new version available.

    $you:wordpress: wp ssh app archive hello-world --host=production
    
When an installed app sees this new version is available, it will prompt the user to visit the app store to install the
update.  Note, for iOS, you will need set the config ios_identifier variable.  See above in the _Packaging for iOS_ 
section.

### App Configuration Variables

You can set all app configuration variables using the `wp app config` command.  When invoked as just `wp app config <slug>`,
then this command acts as a getter for all settings.  When you run it, you will see a list of all variables that can be
set, as well as some information on what they are used for.  By default they are all blank, but you should set all of 
them to something appropriate.  The variable slug is in ( parentheses ).  

    you$wordpress: wp ssh app config hello-world --host=production
    # Bundle Identifier Prefix ( bundle_id )
    # The prefix ( usially reverse domain, i.e. com.domain ) that goes before the bundle identifier for this app.
    com.yourdomain
    
    # Version ( version )
    # The version number to build for the app stores.  Note, the first number ( the "major" version ) is used to determine if an app store update is required for a Hot Code Push.  You should increment the major version only if you are adding in new Cordova plugins.  Anything that touches only the web resources can and should stay in the same major version.
    1.0.0
    
    # Build Number ( build_number )
    # The current build number.  This is set automatically and should not be manually changed.
    0
    
    # Description ( description )
    # Doesn't appear anywhere other than the config.xml file, but fill it in if you want
    This is the description
    
    # Author Name ( author_name )
    # Name of the lead developer on the app.
    Your Name
    
    # Author Email ( author_email )
    # Email of the lead developer on the app.
    you@email.com
    
    # Author URL ( author_url )
    # Url of the lead developer on the app.
    http://yourdomain.com
    
    # iOS Identifier ( ios_identifier )
    # Identification number of the application, for example: id345038631. If defined - used to redirect user to the applications page on iTunes if there is a new version required from a Hot Code Push
    id123456789
    
To set any of them, simply run ( you can set as many variables at once as you want ):

    you$wordpress: wp ssh app config hello-world --bundle_id=com.yourdomain --description="This is the description"

### App Shortcodes

( ToDo - fill this section in with examples on how to build an app and what shortcodes are available to help with that )
