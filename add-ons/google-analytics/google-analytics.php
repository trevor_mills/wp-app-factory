<?php

class AppGoogleAnalytics{
    public function __construct(){
        add_filter('the_app_factory_init',array( &$this, 'init'));
    }

    public function init(& $the_app){
        add_shortcode('google_analytics',array( &$this,'shortcodes'));

        $the_app->register('controller','GoogleAnalytics',dirname(__FILE__).'/the-app/src/controller/GoogleAnalytics.js');
        $the_app->register('helper','GoogleAnalyticsHelper',dirname(__FILE__).'/the-app/src/helper/GoogleAnalyticsHelper.js');

    }

    public function shortcodes( $atts = array(), $content = null, $code = '' ) {
        switch ( $code ) {
        case 'google_analytics':
            $defaults = array(
                'account' => '',
                'domainName' => '',     // Set this to 'none' if you're working on a local domain that isn't called "localhost"
                'debug' => false     // Set to true to console.log() all tracked events.  useful for testing.
            );

            $the_app = the_app();
            $atts = shortcode_atts($defaults,$the_app->sanitize_atts($atts));

            if (empty($atts['account'])){
                wp_die(sprintf(__('In order to use Google Analytics, you must add an `account` attribute with a Google Analytics Account ID (i.e. UA-XXXXXXXX-Y) obtained at %s.  Your shortcode should look like this: %s','app-factory'),'<a href="http://google.com/analytics">google.com/analytics</a>','<br/><pre>[google_analytics account=UA-XXXXXXXX-X]</pre>'));
            }

            $the_app = the_app();

            $the_app->google_analytics = $atts;

            add_filter('the_app_factory_helpers', array( &$this, 'helpers' ),10,2);
            add_action( 'the_app_factory_sencha_package', array( &$this, 'sencha_packager' ) );
            $the_app->enqueue('helper','GoogleAnalyticsHelper');
            $the_app->enqueue('controller','GoogleAnalytics');
            $the_app->enqueue('require','the_app.helper.GoogleAnalyticsHelper');
            $the_app->enqueue('require','the_app.helper.GoogleAnalyticsConfig');
            break;
        }
    }

    public function helpers( $helpers, $args ) {
        $the_app = & $args[0];

        $google_analytics = $the_app->google_analytics;

        $helpers['GoogleAnalyticsConfig'] = $the_app->google_analytics;
        return $helpers;
    }

    /**
     * This method applies to the new Sencha Cmd way of packaging.  See discussion at https://github.com/danwilson/google-analytics-plugin/issues/123
     * Apparently you'll run into problems with the latest version of cordova-plugin-google-analytics unless you have the
     * latest versions of these installed on your dev box:
     *
     *  Android Support Repository
     *  Android Support Library
     *  Google Play Services
     *  Google Repository
     *
     * @return void
     */
    public function sencha_packager()
    {
        $the_app = the_app();
        $the_app->addPlugin( 'cordova-plugin-google-analytics' );
    }

}

new AppGoogleAnalytics();
