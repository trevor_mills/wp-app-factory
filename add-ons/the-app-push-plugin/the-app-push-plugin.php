<?php
/*
Plugin Name: The App Push Plugin
*
*
* Steps for iOS
* 
* 1. Create App Identifier within Apple Developer Console or iTunes Connect
* 2. Follow steps at https://code.google.com/p/apns-php/wiki/CertificateCreation to create Development and/or Distribution .pem files
* 3. Create and install onto device a Provisioning Profile.  Note, the certificate that you include with the profile MUST BE installed on the computer (including the private key)
*/

class TheAppPushPlugin{
    public function __construct(){
        add_action( 'the_app_factory_init', array( &$this, 'init' ) );
        add_filter( 'upload_mimes', array( &$this, 'upload_mimes' ) );
    }

    public function init( &$the_app ){
        add_shortcode( 'app_push_plugin', array( &$this, 'shortcodes' ) );

        $the_app->register( 'controller', 'PushPluginController', dirname(__FILE__) . '/the-app/src/controller/PushPluginController.js' );
    }

    public function shortcodes( $atts = array(), $content = null, $code = '' ){
        $the_app = the_app();

        $the_app->pushplugin_atts = $this->getPushPluginSettings() ;
        $the_app->enqueue( 'controller', 'PushPluginController' );

        add_action( 'the_app_factory_setup_helpers', array( &$this, 'helper' ) );
        add_action( 'the_app_factory_sencha_package', array( &$this, 'sencha_packager' ) );
    }

    public function getPushPluginSettings(){
        $the_app = the_app();

        $app_meta = the_app_get_app_meta( $the_app->post->ID );

        $pushplugin_atts = isset( $app_meta[ 'pushplugin' ] ) ?
            $app_meta[ 'pushplugin' ] :
            array(
                'google_api_key' => null,
                'google_project_number' => null
            );
        if ( !isset( $pushplugin_atts['pem'] ) ){
            $pushplugin_atts['pem'] = array(
                'sandbox' => '',
                'production' => '',
                //'entrust' => ''
            );
        }

        $pushplugin_atts[ 'app_api_key' ] = PushPluginApi::getApiKey();
        return $pushplugin_atts;
    }

    public function helper( &$the_app ){
        $helpers = $the_app->helpers;
        $helpers['PUSHPLUGIN'] = wp_parse_args( $the_app->pushplugin_atts, array(
            // Only publish these keys
            'google_api_key' => '',
            'google_project_number' => '',
            'app_api_key' => ''
        ) );
        $the_app->helpers = $helpers ;
        $the_app->enqueue( 'require', 'the_app.helper.PUSHPLUGIN');
    }

    public function upload_mimes( $mimes ){
        $mimes[ 'pem' ] = 'application/x-pem-file';
        return $mimes;
    }

    /**
     * This method applies to the new Sencha Cmd way of packaging
     *
     * @return void
     */
    public function sencha_packager()
    {
        $the_app = the_app();
        $the_app->addPlugin( 'https://github.com/phonegap-build/PushPlugin.git' );
    }



}

new TheAppPushPlugin();

include_once( 'class.PushPluginApi.php' );
include_once( 'class.PushPluginAdmin.php' );
