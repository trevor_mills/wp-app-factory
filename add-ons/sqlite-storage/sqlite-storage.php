<?php
/*
Plugin Name: SqliteStorage
*/

class SqliteStorage{
    public function __construct(){
        add_action( 'the_app_factory_parse_post', array( &$this, 'parsePost' ) );
    }

    public function parsePost( &$the_app ){
        if ( $the_app->is_using_manifest && $the_app->storage_engine == 'sqlitestorage' ){
            $this->setupSQLiteStorage();
        }
        else{
            add_action( 'the_app_factory_setup_stores', array( &$this, 'maybeIncludeSQLiteStorage') );
        }

    }

    public function maybeIncludeSQLiteStorage( & $the_app ){
        $stores = $the_app->stores;
        foreach ( $stores as $store ){
            if ( $store['storage_engine'] == 'sqlitestorage' ){
                $this->setupSQLiteStorage();
                break;
            }
        }
    }

    public function setupSQLiteStorage(){
        add_action( 'the_app_factory_sencha_package', array( &$this, 'sencha_packager' ) );

        $the_app = the_app();
        $the_app->enqueue( 'require', 'SqliteDemo.util.InitSQLite' );
        $the_app->enqueue( 'require', 'Sqlite.data.proxy.SqliteStorage' );
        $the_app->enqueue( 'require', 'Sqlite.Connection' );
    }

    /**
     * This method applies to the new Sencha Cmd way of packaging
     *
     * @return void
     */
    public function sencha_packager()
    {
        $the_app = the_app();
        $the_app->addPlugin( 'cordova-sqlite-storage' );
    }

}

new SqliteStorage();
