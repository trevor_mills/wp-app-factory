<?php
/*
Plugin Name: WP App Factory
Plugin URI: http://topquark.com/wp-app-factory/extend/plugins/wp-app-factory
Description: Creates a cross-device mobile app out of any post type using Sencha Touch as the framework
Version: 3.0.0
Author: Top Quark
Author URI: http://topquark.com

Copyright (C) 2011 Trevor Mills (support@topquark.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Note: This plugin is distributed with a debug and production version of
Sencha Touch, which is also released under GPLv3.  
See http://www.sencha.com/products/touch/license/

*/

// Define a couple of constants that will be useful
define( 'APP_FACTORY_URL', trailingslashit( plugins_url( basename( dirname( __FILE__ ) ) ) ) );
define( 'APP_FACTORY_PATH', trailingslashit( dirname( __FILE__ ) ) );

/**
 * Our autoload function.  Looks in the lib directory for matching classes.
 *
 * @param string $class an as-yet undefined class
 *
 * @return void
 */
function wp_app_factory_autoload( $class )
{
    if ( file_exists( APP_FACTORY_PATH . "lib/$class.class.php" ) ) {
        include_once APP_FACTORY_PATH . "lib/$class.class.php";
    }
}
spl_autoload_register( 'wp_app_factory_autoload' );

/**
 * If running from the command line using the WordPress CLI ( @see https://github.com/wp-cli/wp-cli ), then setup the
 * command class.
 */
if ( defined( 'WP_CLI' ) && WP_CLI ) {
    WP_CLI::add_command( 'app', 'TheAppCommand' ); // will be found via our autoloader
    include_once APP_FACTORY_PATH . 'lib/wp-cli-ssh.php';
}

/**
 * Setup The App Factory.  Register the WordPress post type, define some constants, setup a handful of hook listeners,
 * load add-ons.  Run on the WordPress 'init' action.
 *
 * @return void
 */
function the_app_factory_init()
{
    // Defining the App Custom Post Type
    $labels = array(
        'name' => _x('Apps', 'post type general name'),
        'singular_name' => _x('App', 'post type singular name'),
        'add_new' => _x('Add New', 'apps'),
        'add_new_item' => __('Add New App'),
        'edit_item' => __('Edit App'),
        'new_item' => __('New App'),
        'all_items' => __('All Apps'),
        'view_item' => __('View App'),
        'search_items' => __('Search Apps'),
        'not_found' =>  __('No apps found'),
        'not_found_in_trash' => __('No apps found in Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Apps'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','author','thumbnail','permalink','revisions')
    );

    if (!defined('APP_POST_TYPE')){
        define('APP_POST_TYPE','apps');
    }
    if (!defined('APP_DATA_VAR')){
        define('APP_DATA_VAR','the_data');
    }
    /*
    if (!defined('APP_POST_VAR')){
        define('APP_POST_VAR','app');
    }
    if (!defined('APP_MANIFEST_VAR')){
        define('APP_MANIFEST_VAR','the_manifest');
    }
    if (!defined('APP_APPSCRIPT_VAR')){
        define('APP_APPSCRIPT_VAR','the_script');
    }
    if (!defined('APP_JSON_VAR')){
        define('APP_JSON_VAR','json');
    }
    if (!defined('APP_JS_VAR')){
        define('APP_JS_VAR','js');
    }
    if (!defined('APP_COMMAND_VAR')){
        define('APP_COMMAND_VAR','command');
    }
    */
    if (!defined('APP_APP_VAR')){
        define('APP_APP_VAR','app_file');
    }

    register_post_type(APP_POST_TYPE,$args);

    add_action( 'template_redirect', 'the_app_factory_redirect', 1);

    add_shortcode( 'app_package_image', array( 'TheAppSenchaPackager','package_image' ) );
    add_action( 'wp_ajax_prepare_sencha', array( 'TheAppSenchaPackager', 'prepare_sencha' ) );
    add_action( 'wp_ajax_prepare_sencha_files', array( 'TheAppSenchaPackager', 'prepare_sencha' ) );
    add_action( 'wp_ajax_download_sencha_package', array( 'TheAppSenchaPackager', 'prepare_download' ) );
    add_action( 'save_post', array( 'TheAppMeta', 'save_postdata' ) );

    // Load some plugins
    foreach ( glob( APP_FACTORY_PATH . 'add-ons/*' ) as $directory ) {
        $path = $directory . '/' . basename( $directory ) . '.php';
        if ( file_exists( $path ) ) {
            include_once $path;
        }
    }
    do_action( 'the_app_factory_load_plugins' );
}
add_action('init', 'the_app_factory_init');

/**
 * For the WordPress backend, when editing an app, load in these custom meta boxes.  Run on the 'admin_init' action
 *
 * @return void
 */
function the_app_factory_admin_init(){
    add_meta_box( 'the_app_settings', __('App Settings','app-factory'), 'TheAppMeta::settings_metabox', APP_POST_TYPE, 'normal', 'high' );
    add_meta_box( 'the_app_sencha_package', __('Package With Sencha','app-factory'), 'TheAppSenchaPackager::package_metabox', APP_POST_TYPE, 'normal', 'high' );
}
add_action('admin_init','the_app_factory_admin_init');

/**
 * Get app meta for the supplied $id ( a post id ).  Just a wrapper to TheAppMeta::getSettings()
 *
 * @param int $id a post id of an app post
 *
 * @return array any postmeta associated with the app.
 */
function the_app_get_app_meta( $id )
{
    return TheAppMeta::getSettings( $id );
}

/**
 * If requesting an app at the default WordPress permalink ( ie. http://mydomain.com/apps/hello-world ), then redirect
 * to the Sencha Packaged version, if it exists ( @TODO )
 *
 * @return void
 */
function the_app_factory_redirect()
{
    global $wp;
    if ( preg_match( '#^' . APP_POST_TYPE . '/([^/]+)/?(.*)$#', $wp->request, $matches ) ) {
        $post = get_page_by_path( $matches[1], OBJECT, APP_POST_TYPE );
        if ( $post ) {
            $the_app = TheAppFactory::instantiateFromPost( $post, 'TheAppSenchaPackager' );

            if ( is_dir( $the_app->archive_root . 'www' ) ) {
                $root     = $the_app->archive_root . 'www/';
                $root_url = $the_app->archive_root_url . 'www/';
            } elseif ( is_dir( $the_app->package_native_root . 'cordova/www' ) ) {
                $root     = $the_app->package_native_root . 'cordova/www/';
                $root_url = $the_app->package_native_root_url . 'cordova/www/';
            } else {
                wp_die( __( 'The app has not been built yet', 'app-factory' ) );
            }

            if ( empty( $matches[2] ) ) {
                // And the app has been archived or packaged, redirect to it
                wp_redirect( $root_url );
                exit;
            } elseif ( file_exists( $root . $matches[2] ) ) {
                $extension = pathinfo( $matches[2], PATHINFO_EXTENSION );
                switch( $extension ) {
                case 'json':
                    $content_type = 'application/json';
                    break;
                case 'js':
                    $content_type = 'text/javascript';
                    break;
                case 'css':
                    $content_type = 'text/css';
                    break;
                case 'manifest':
                default:
                    $content_type = 'document';
                    break;
                }
                header( "Content-type: $content_type" );
                http_response_code( 200 );
                readfile( $root . $matches[2] );
                exit;
            // } elseif ( preg_match( '#^chcp/(.*)$#', $matches[2], $chcp ) ) {
            //     http_response_code( 200 );
            //     echo $the_app->getHotCodePushData( $chcp[1] ); // will set the content-type header and echo out the appropriate response
            //     exit;
            } elseif ( preg_match( '#^data/(.*)$#', $matches[2], $data ) ) {
                // It's a data request
                http_response_code( 200 );
                echo $the_app->getData( $data[1] ); // will set the content-type header and echo out the appropriate response
                exit;
            } else {
                $the_app->requested_endpoint = $matches[2];
                do_action_ref_array( 'the_app_factory_redirect', array( & $the_app ) );
            }
        }
    }
}

/**
 * Allow uploading of CSS files to the media library.  Called in the 'upload_mimes' filter.
 *
 * @param array $existing_mimes - the currently allowed mimes
 *
 * @return array allowed mimes, with 'css' added
 */
function the_app_upload_mimes ( $existing_mimes=array() )
{
    // add the file extension to the array
    $existing_mimes['css'] = 'text/css';
    // call the modified list of extensions 
    return $existing_mimes;
 
}
add_filter( 'upload_mimes', 'the_app_upload_mimes' );

/**
 * This was an interesting one.  When loading the data or the manifest WP_Query thinks that it's on the home page.
 * Some plugins (The Events Calendar, for example) might suppress certain categories on the home page.  I don't want
 * that.  I want The App Factory to have complete control over its query.
 *
 * I don't know if this is strictly still necessary in the new Sencha packaged paradigm.
 *
 * @param object $query the query - note it is passed by reference
 *
 * @return void
 */
function the_app_parse_query( &$query )
{
    if ( defined( 'APP_POST_VAR' ) && get_query_var( APP_POST_VAR ) and $query->is_home ) {
        $query->is_home = false;
    }
}
add_action( 'parse_query', 'the_app_parse_query', 1 );

/**
 * When deleting an attachment, delete any images that the app might have made for the attachment.  Called in the
 * 'delete_attachment' action.
 *
 * @param int $id the attachment id
 *
 * @return void
 */
function the_app_delete_attachement_app_images( $id )
{
    $app_images = get_post_meta( $id, 'app_made_images', true );
    if ( is_array( $app_images ) ) {
        foreach ( $app_images as $file ) {
            if( file_exists( $file ) ) {
                @unlink( $file );
            }
        }
    }
}
add_action( 'delete_attachment', 'the_app_delete_attachement_app_images' );

/**
 * Use Javascript to add in the ability to have an app appear on the front page of the site.  This isn't possible with
 * WordPress hooks, so I did it in Javascript.
 *
 * @return void
 */
function the_app_admin_print_scripts(){
    global $pagenow;
    if ( $pagenow == 'options-reading.php' ) {
         wp_enqueue_script( 'wp-app-factory.options-reading', APP_FACTORY_URL.'resources/js/wp-app-factory.options-reading.js', 'jquery' );
        $app_posts = get_posts( 'post_type=' . APP_POST_TYPE );
        $apps = array();
        foreach ( $app_posts as $app ) {
            $apps[] = array(
                'id' => $app->ID,
                'title' => $app->post_title
            );
        }

        // Thanks http://wordpress.stackexchange.com/questions/36551/create-a-dropdown-with-custom-post-types-as-option-in-admin
        function generate_post_select($select_id, $post_type, $selected = 0) {
            $post_type_object = get_post_type_object($post_type);
            $label = $post_type_object->label;
            $posts = get_posts(array('post_type'=> $post_type, 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
            $return = '';
            $return.= '<select name="'. $select_id .'" id="'.$select_id.'">';
            $return.= '<option value="0" >'.__( '&mdash; Select &mdash;' ).'</option>';
            foreach ($posts as $post) {
                $return.= '<option value="'. $post->ID. '"'. ($selected == $post->ID ? ' selected="selected"' : ''). '>'. $post->post_title. '</option>';
            }
            $return.= '</select>';
            return $return;
        }

        $localized = array(
            'apps' => $apps,
            'show_on_front' => get_option( 'show_on_front' ),
            'show_on_front_message' => sprintf(__('An %sapp%s (select below).  Note: by choosing this option, all pages (including 404), lead to this app.  Use it if you would like your WordPress installation to ONLY host this app.','app-factory'),'<a href="'.admin_url('edit.php?post_type='.APP_POST_TYPE).'">','</a>'),
            'app' => __('App','app-factory'),
            'dropdown' => generate_post_select('app_on_front',APP_POST_TYPE,get_option( 'app_on_front' )),
            'app_post_type' => APP_POST_TYPE
        );
        wp_localize_script( 'wp-app-factory.options-reading', 'WP_APP_FACTORY', $localized );
    }
}
add_action('admin_print_scripts','the_app_admin_print_scripts',20);

/**
 * Add in our app_on_front optiona as a whitelisted option.
 *
 * @param array $whitelist_options current whitelisted items
 *
 * @return array now app_on_front is valid for the 'reading' options
 */
function the_app_whitelist_options($whitelist_options){
    $whitelist_options['reading'][] = 'app_on_front';
    return $whitelist_options;
}
add_filter('whitelist_options','the_app_whitelist_options');

/* Prevent unauthorized users from being able to see the source code for the app */
add_filter( 'the_content', 'the_app_the_content' );
function the_app_the_content( $content ){
    if ( APP_POST_TYPE == get_post_type() && is_archive() ) {
        $content = '<a href="' . get_permalink() . '">' . sprintf( __( 'View "%s" app here', 'app-factory' ), get_the_title() ) . '</a>';
    }
    return $content;
}

/**
 * A helper function that saves you from having to type $the_app = & TheAppFactory::getInstance().  It returns a reference
 * the the current TheAppFactory singleton, which may be a TheAppFactory or TheAppSenchaPackager
 * depending on the context.  The point is that whatever context you're writing code in, the_app() returns the right
 * object.  Does it actually work that way?  Yeah, probably 90% or more of the way you expect it to.
 *
 * Usage:
 * $the_app = the_app();
 * @return TheAppFactory|TheAppSenchaPackager
 */
function & the_app()
{
    return TheAppFactory::getInstance();
}

?>