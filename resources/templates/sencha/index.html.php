<?php
$the_app = the_app();
$post = $the_app->post;
?>
<!DOCTYPE HTML>
<html manifest="" lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo $post->post_title; ?></title >
    <?php do_action('the_app_factory_print_stylesheets'); ?>
    <?php do_action('the_app_factory_print_scripts'); ?>
    <!-- The line below must be kept intact for Sencha Command to build your application -->
    <script id="microloader" type="text/javascript" src=".sencha/app/microloader/development.js"></script>
    <script type="text/javascript">var PACKAGED_APP=true;</script>
</head>
<body class="<?php echo implode(' ', apply_filters( 'the_app_factory_body_class', array( 'theme-' . $the_app->theme ) ) ); ?>">
<div id="app-loading"></div>
</body>
</html>