<?php
    $the_app = the_app();
    $items = $the_app->the_items();

    foreach ($items as $key => $item){
        $items[$key] = array(
            'xtype' => 'lazypanel',
            'originalItem' => $item
        );

    }

    if ($the_app->is_using_manifest){
        $the_app->enqueue('require','Ext.ux.OfflineSyncStore');
        $the_app->enqueue('require','Ext.ux.OfflineSyncStatusStore');
        $the_app->enqueue('require','My.data.proxy.LocalStorage');
    }

?>
// Doing this greatly improves the performance of Ext.decode
Ext.USE_NATIVE_JSON = true;

// Disable the _dc=<timestamp> caching of the SDK files.
Ext.Loader.setConfig({disableCaching:false});

<?php foreach ( $the_app->registered['path'] as $name ) :
    /**
     * This bit of code will register the path of a non-sdk file so that when the app tries to load it, Sencha knows
     * where it is.  To get something to output here from PHP land, you would do the following:
     *
     *     $the_app = the_app();
     *     $the_app->register( 'path', 'My.controller.MyAwesomeController','/filesystem/path/to/controller/MyAwesomeController.js' );
     *     $the_app->enqueue( 'controller', 'My.controller.MyAwesomeController' );
     */
    $path = $the_app->registered['paths']['path'][$name]; // The file path to the file
    $category = basename( dirname( $path ) ); // If path is /path/to/it/does/not/matter/until/CATEGORY/file.js, then $category is CATEGORY
    $packaged_path = "app/$category/" . basename( $path ); // Where the file will end up with after packaging
    ?>
    Ext.Loader.setPath( '<?php echo $name; ?>', '<?php echo $packaged_path; ?>' );
<?php endforeach; ?>

Ext.application({
    name: 'the_app',

    models: <?php $the_app->render('models'); ?>,
    views: <?php $the_app->render('view'); ?>,
    controllers: <?php $the_app->render('controller'); ?>,
    stores: <?php $the_app->render('stores'); ?>,
    profiles: <?php $the_app->render('profile'); ?>,
    requires: <?php $the_app->render('require'); ?>,

    meta: <?php $the_app->render('meta'); ?>,

    eventPublishers: {
        touchGesture: {
            moveThrottle: 10
        }
    },

    isIconPrecomposed: true,

    viewport: {
        // Changed this based on comments at https://www.sencha.com/forum/showthread.php?164247-quot-Timeout-waiting-for-window.innerHeight-to-quot-error-when-running-the-application-on-iOS/page2
        autoMaximize: Ext.os.is.iOS && Ext.browser.is.webview
    },

    launch: function(){
        Ext.Viewport.add({
            xtype: 'launcher',
            title: <?php echo json_encode($the_app->title); ?>,
            fullscreen: true,
            <?php if ( 'sheet' == $the_app->menu_style ) : ?>
            sheetMenuItems: <?php echo TheAppFactory::anti_escape( json_encode( $the_app->sheet_menu_items ) );  ?>,
            <?php endif; ?>
            <?php if ($the_app->ios_install_popup) : ?>
            installApp: true,
            <?php endif; ?>
            mainItems: <?php echo TheAppFactory::anti_escape(json_encode( $items )); ?>
        });
    },

    confirm: function( options ){
        Ext.apply( options, {
            buttons: [
                {
                    xtype: 'button',
                    text: WP.__('No'),
                    handler: function( button ){
                        var handler = options.handlerNo || Ext.emptyFn;
                        handler.apply( this );
                        this.hidePopup( button.up( 'panel' ).getId() );
                    },
                    scope: this
                },
                {
                    xtype: 'button',
                    text: WP.__('Yes'),
                    handler: function( button ){
                        var handler = options.handler || Ext.emptyFn;
                        handler.apply( this );
                        this.hidePopup( button.up( 'panel' ).getId() );
                    },
                    scope: this
                }
            ]
        });
        this.showPopup( options );
    },

    alert: function( options ){
        Ext.apply( options, {
            hideOnMaskTap: true,
            buttons: [
                {
                    xtype: 'button',
                    text: options.buttonText ? options.buttonText : WP.__('Ok'),
                    handler: function( button ){
                        this.hidePopup( button.up( 'panel' ).getId() );
                    },
                    scope: this
                }
            ]
        });
        this.showPopup( options );
    },

    showPopup: function( options ){
        var config = {
            xtype: 'panel',
            modal: true,
            hideOnMaskTap: (options.hideOnMaskTap ? true : false),
            hidden: true,
            centered: true,
            width: (options.width  ? options.width : 300),
            height: (options.height  ? options.height : 200),
            cls: 'popup',
            hideAnimation: {
                type: 'popOut',
                duration: 250,
                easing: 'ease-out',
                listeners: {
                    animationend: function(evt, obj) {
                        this.removePopup( obj.id );
                    },
                    scope: this
                }
            },
            showAnimation: {
                type: 'popIn',
                duration: 250,
                easing: 'ease-out',
                listeners: {
                    animationend: function(evt, obj) {
                    }
                }
            },
            items: [],
            zIndex: 10000
        };
        if ( options.id ){
            config.cls += ' ' + options.id;
            config.id = options.id;
            var existing = Ext.getCmp( config.id );
            if ( existing ){
                this.removePopup( existing );
            }
        }

        if ( typeof options.showAnimation != 'undefined' ){
            config.showAnimation = options.showAnimation;
        }

        if ( this.last_popup ){
            this.last_popup.hide();
            config.showAnimation = false;
        }

        if ( options.title ){
            config.items.push( {
                xtype: 'toolbar',
                docked: 'top',
                title: options.title
            });
        }
        if ( !options.html && options.message ){
            options.html = options.message;
        }
        if ( options.html ){
            config.items.push( {
                xtype: 'component',
                html: options.html + (options.spinner ? '<div class="spinner ' + options.spinner + '"></div>' : '' ),
                styleHtmlContent: true,
                scrollable: 'vertical'
            });
        }
        if ( options.buttons ){
            config.items.push( {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                },
                items: options.buttons
            });
        }

        this.last_popup = Ext.Viewport.add( config );
        this.last_popup.show();
    },

    hidePopup: function( id ){
        this.removePopup( id );
    },

    removePopup: function( cmp ){
        if ( Ext.isString( cmp ) ){
            cmp = Ext.getCmp( cmp );
        }
        if ( !cmp ){
            return;
        }
        cmp.destroy();
        // For some reason, ST leaves masks lying around.  I'm going to clean them up
        Ext.each( Ext.Viewport.query( 'mask' ), function( mask ){
            mask.destroy();
        });
        this.last_popup = false;
    },

    onUpdated: function() {
        the_app.app.confirm(
            {
                id: 'update',
                title: WP.__("Application Update"),
                html: WP.__("This application has just successfully been updated to the latest version. Reload now?"),
                hideOnMaskTap: false,
                handler: function(){
                    window.location.reload();
                }
            }
        );
    }
});
