<?php

class TheAppSenchaPackager extends TheAppFactory
{

    /**
     * A method to execute a shell command after first "source"ing the user's .bash_profile file
     *
     * @param string $command the command to be executed
     * @param bool $all if false, it just returns the first line of output, otherwise, it returns everything.
     *
     * @return string|null the response from the command
     */
    public function exec( $command, $all = false )
    {
        exec( ( $this->isWindows() ? '' : "source ~/.bash_profile && " ) . "$command", $output, $return_var );
        return $all ? $output : $output[0];
    }

    /**
     * Similar to exec, but this function just automatically echoes out the result to the browser
     *
     * @param string $command the command to be executed
     * @param bool   $halt    if true, the build process will show an error
     *
     * @return void
     */
    public function passthru( $command, $halt = true )
    {
        passthru( ( $this->isWindows() ? '' : "source ~/.bash_profile && " ) . "$command", $return );
        if ( $halt && $return !== 0 ) {
            $this->error( sprintf( "The command `%s` returned a non-zero value of %d.  Halting build.", $command, $return ) );
            exit;
        }
    }

    public function error( $message )
    {
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            WP_CLI::warning( $message );
        } else {
            $this->flush( '<div class="description" style="overflow:auto;padding:10px;background:#ccc;border:1px solid #999">ERROR:<br/>' . $message . '</div>' );
        }
    }

    /**
     * Are we in a Windows environment, or linux
     *
     * @return boolean
     */
    public function isWindows()
    {
        return ( strtoupper( substr( PHP_OS, 0, 3 ) ) === 'WIN' );
    }

    /**
     * A helper method to know if the `sencha` command is installed on the server
     */
    public function isAvailable()
    {
        $result = $this->exec( $this->isWindows() ? 'where sencha' : 'which sencha' );
        if ( !isset( $result ) ) {
            $this->error( 'Could not find `sencha` on the path: ' . $this->exec( $this->isWindows() ? 'echo %PATH%' : 'echo $PATH' ) );
            return false;
        }
        $defaults = $this->get_default_atts( 'the_app' );
        $var = 'SENCHA_SDK_' . str_replace( '.', '', $defaults['sdk'] );
        $result = $this->exec( $this->isWindows() ? 'echo %' . $var . '%' : 'echo $' . $var );
        if ( empty( $result ) ) {
            $this->error( "You must have an environment variable called $var defined. Note: .bash_profile is your friend." );
            return false;
        }
        $result = $this->exec( $this->isWindows() ? 'where cordova' : 'which cordova' );
        if ( !isset( $result ) ) {
            $this->error( 'Could not find `cordova` on the path: ' . $this->exec( $this->isWindows() ? 'echo %PATH%' : 'echo $PATH' ) );
            return false;
        }
        return true;
    }

    /**
     * Setup the environment for the Sencha Packaging
     */
    public function setupEnvironment()
    {
        $post = $this->post;
        if ( !$post ) {
            global $post;
            $this->post = $post ;
        }
        $uploads = wp_upload_dir();

        // Some vars for packaging
        $this->package_root = trailingslashit( $uploads['basedir'] ) ; // multisite safe - uploads or files directory
        $this->package_root_url = trailingslashit( $uploads['baseurl'] ) ;
        $this->package_name = preg_replace( '/[^a-zA-Z0-9]/', '', $post->post_title ) ;

        // Are we actually packaging and is there a target set
        $targets = array( 'sencha' => __( 'Sencha', 'app-factory' ) ); // $this->get_available_targets();
        $this->package_target = 'sencha' ;
        $this->packaging = true;

        $relative = trailingslashit( "wp-app-factory/build/sencha/$post->post_name" );
        $this->package_native_root = $this->package_root . $relative;
        $this->package_native_root_url = $this->package_root_url . $relative;
        $this->package_native_www_relative = ''; // in the new paradigm, the www root is actually the app root, so a blank string is appropriate.  It used to be 'www/';
        $this->package_native_www = $this->package_native_root . $this->package_native_www_relative;

        $relative = trailingslashit( "wp-app-factory/archive/sencha/$post->post_name" );
        $this->archive_root = $this->package_root . $relative;
        $this->archive_root_url = $this->package_root_url . $relative;

        if ( $this->isWindows() ) {
            foreach ( array( 'package_root', 'package_native_root' ) as $root ) {
                $this->$root = str_replace( '/', DIRECTORY_SEPARATOR, $this->$root );
            }
        }

        $this->environment = 'sencha' ;
        $this->is_packaging = true;
    }

    public static function package_metabox( $app )
    {
        $the_app = & TheAppFactory::instantiateFromPost( $app, 'TheAppSenchaPackager' );

        $app_meta = $the_app->getSettings();

        ?>
        <p><?php echo __( 'Packaging your app for Sencha sidesteps all of WP App Factory\'s misgivings of including Sencha Touch directory within it and instead uses Sencha CMD to stand up the app.', 'app-factory' ); ?></p>

        <?php if ( !$the_app->isAvailable() ) : ?>
            <p><?php echo sprintf( __( 'This option is not currently available.  Please make sure that when you shell into the server as %s that the `sencha` command is available.  If you need to add it to your path, please add it in .bash_profile.', 'app-factory' ), $the_app->exec( 'whoami' ) ); ?></p>
            <p><?php echo sprintf( __( 'Additionally, you must have an environment variable exported called SENCHA_SDK_%s that is the full path to the Sencha Touch SDK for version %s on the server.  Again, .bash_profile is your friend.', 'app-factory' ), str_replace( '.', '', $defaults['sdk'] ), $defaults['sdk'] ); ?></p>
        <? else : ?>
            <p><?php echo __( 'The packaged app is delivered as a .zip file that contains a project that you can open and run in XCode (iOS) or build using the Android SDK (Android).', 'app-factory' ); ?></p>

            <?php if ( !empty( $app_meta['bundle_id'] ) ) : ?>
                <a href="#" id="prepare-sencha" class="button-primary"
                   target="_blank"><?php echo __( 'Prepare Clean Sencha ( be patient )', 'app-factory' ); ?></a>
                <?php if ( is_dir( $the_app->package_native_root ) ) : ?>
                    <a href="#" id="prepare-sencha-files" class="button-primary"
                       target="_blank"><?php echo __( 'Prepare Just Sencha Files', 'app-factory' ); ?></a>
                    <a href="<?php echo admin_url( 'admin-ajax.php' ); ?>?action=download_sencha_package&slug=<?php echo $app->post_name; ?>" class="button-primary"
                        ><?php echo __( 'Download Package ZIP', 'app-factory' ); ?></a>
                <?php endif; ?>
                <span class="spinner" style="float:none"></span>
                <iframe id="prepare-sencha-output"
                        style="overflow:auto;max-height:200px;border:1px solid #ccc;display:none;margin-top:1em;width:100%;box-sizing:border-box"></iframe>
                <script type="text/javascript">
                    jQuery(function ($) {
                        $('#prepare-sencha,#prepare-sencha-files').click(function (e) {
                            e.preventDefault();
                            var $button = $(this);
                            if ($button.is(':disabled')) {
                                return;
                            }
                            $button.add($button.siblings('.button-primary')).attr('disabled', 'disabled');
                            $button.siblings('.spinner').css('visibility', 'visible');

                            $('#prepare-sencha-output').show().attr('src', 'about:blank').attr(
                                'src',
                                [
                                    ajaxurl,
                                    $.param({
                                        action: $button.is('#prepare-sencha') ? 'prepare_sencha' : 'prepare_sencha_files',
                                        slug: <?php echo json_encode( $app->post_name ); ?>,
                                    })
                                ].join('?')
                            );

                            $('#prepare-sencha-output').one('complete', function () {
                                $button.add($button.siblings('.button-primary')).removeAttr('disabled');
                                $button.siblings('.spinner').css('visibility', 'hidden');
                            });
                        })
                    });
                </script>
                <?php
            endif;
        endif;
    }

    /**
     * Run the process to take a WordPress app post and turn it into a Sencha app environment.  The Sencha environment
     * will be put into the wp-content/uploads/wp-app-factory/build/sencha/<app-slug> directory and any requests to
     * http://thisdomain.com/apps/app-slug will redirect to the Sencha environment.
     *
     * @param null|string $action the action to perform.  Can be either 'prepare_sencha' to do a full clean build or
     *                            prepare_sencha_files to just copy over the files
     *
     * @return void
     */
    public static function prepare_sencha( $action = null )
    {
        if ( empty( $action ) ) {
            $action = $_REQUEST['action'];
        }

        set_time_limit( 0 ); // this could take a while

        global $post;
        $post = apply_filters( 'the_app_factory_sencha_get_post', get_page_by_path( $_REQUEST['slug'], OBJECT, APP_POST_TYPE ) );

        $the_app = &TheAppFactory::instantiateFromPost( $post, __CLASS__ );

        echo '<div style="white-space:pre-wrap;font-family:courier,monospace">';
        echo 'Building app in ... ';
        for ( $c = 5; $c > 0; $c-- ) {
            echo "$c ";
            flush();
            usleep( 100000 );
        }
        $the_app->flush( 'GO!!!' );

        try {
            $the_app->prepare_sencha_steps( $action );
        }
        catch ( Exception $e ) {
            $this->error( $e->getMessage() );
            $the_app->flush( '</div>' );
            return;
        }
    
        $the_app->flush( '</div>' );

        $the_app->flush(
            '
            <script type="text/javascript">
                if ( typeof top != "undefined" && typeof top.jQuery != "undefined" ) {
                    top.jQuery("#prepare-sencha-output").trigger("complete");
                }
            </script>
            '
        );

        exit;
    }

    /**
     * The actual steps involved in preparing the sencha package.
     *
     * @param $action either 'prepare_sencha' or 'prepare_sencha_files'
     *
     * @return void
     */
    public function prepare_sencha_steps( $action )
    {
        $this->clean_build = ( $action == 'prepare_sencha' );

        // Increment the build number
        $settings = $this->getSettings();
        $settings['build_number']++;
        $this->saveSettings( $settings );

        // Only do these expensive operations if requested.
        if ( $this->clean_build ) {
            // remove the current packaged environment so we start fresh
            $this->flush( "# Removing old prepared directory ( if exists )" );
            $this->rrmdir( $this->package_native_root );

            // Step 1: create the Sencha Touch App
            $this->flush( "# Generating the Sencha App ( be patient )" );
            $this->passthru( sprintf( 'sencha -sdk ' . ( $this->isWindows() ? '%%SENCHA_SDK_%s%%' : '$SENCHA_SDK_%s' ) . ' generate app %s %s', str_replace( '.', '', $this->sdk ), $this->package_name, $this->package_native_root ) );
            // Remove these unnecessary files ( by just removing the directories )
            $this->rrmdir( "$this->package_native_root/resources/icons" );
            $this->rrmdir( "$this->package_native_root/resources/loading" );
            $this->rrmdir( "$this->package_native_root/resources/sass" );
            $this->rrmdir( "$this->package_native_root/resources/startup" );

            // Hacks.
            // The app includes Ext.log.Logger.  In order to get that into the production build, the following must be added to .sencha/app/production.properties: build.options.logger=yes
            file_put_contents( $this->package_native_root . '.sencha/app/production.properties', "\nbuild.options.logger=yes\n", FILE_APPEND );
            // Set this project up to SKIP SASS compilation.  If you ever put this back in, you'll need to keep the resources/sass directory that otherwise gets blown away with the $this->rrmdir( "$this->package_native_root/resources/sass" ) line above
            file_put_contents( $this->package_native_root . '.sencha/app/sencha.cfg', "\nskip.sass=true\n", FILE_APPEND );

        }

        // Step 2: Update the index.html file
        $this->flush( "# Generating the index.html file" );
        $this->generateIndex();

        // Step 3: Update the app.js file
        $this->flush( "# Generating the app.js file" );
        $this->generateAppJs();

        // Step 4: Copy over required JS files:
        $this->flush( "# Copying over required app js files" );
        $this->copyRequiredJsFiles();

        // Step 5: Update the app.json File
        $this->flush( "# Generating app.json" );
        $this->generateAppJson();

        // Step 6: Deploy data
        $this->flush( "# Deploying data" );
        $this->deployData();

        // Step 7: deploy images
        if ( !empty( $this->image_queue ) ) {
            $this->deploy_queued_images( false );
        }
        if ( $this->icon ) {
            $this->deploy_icons( $this->icon );
        }
        if ( $this->startup_phone ) {
            $this->deploy_startup_phone( $this->startup_phone );
        }
        if ( $this->startup_tablet ) {
            $this->deploy_startup_tablet( $this->startup_tablet );
        }
        if ( $this->startup_landscape_tablet ) {
            $this->deploy_startup_landscape_tablet( $this->startup_landscape_tablet );
        }
        if ( $this->startup_portrait && $this->startup_landscape ) {
            $this->deploy_startup_images( $this->startup_portrait, $this->startup_landscape );
        }
        do_action( 'the_app_factory_sencha_package_resources' );

        // Only do this expensive operations if requested.
        if ( $this->clean_build ) {
            $this->flush( "# Adding in Cordova ( be patient )" );
            $app_meta = the_app_get_app_meta( $this->post->ID );
            $name = $this->package_name;
            $app_identifier = $app_meta['bundle_id'] . '.' . $name;
            $this->passthru( sprintf( 'cd %s && sencha cordova init %s', $this->package_native_root, $app_identifier ) );

            $this->addPlugin( 'cordova-plugin-console' );
            $this->addPlugin( 'cordova-plugin-inappbrowser' );
            if ( $this->is_using_hot_code_push ) {
                $this->addPlugin( 'cordova-hot-code-push-plugin' );
            }

            do_action( 'the_app_factory_sencha_package' );
        }

        // Step 8: adjust config.xml
        $this->flush( '# Adjusting Config XML' );
        $this->adjustConfigXML();

        $this->flush( "# Refreshing App Metadata ( be patient )" );
        $this->rrmdir( "{$this->package_native_root}cordova/www" );
        $this->passthru( sprintf( 'cd %s && sencha app build cordova', $this->package_native_root ) );

        // Create the chcp.json & chcp.manifest files, as need be
        if ( $this->is_using_hot_code_push ) {
            $this->passthru( sprintf( 'cd %s && cordova-hcp build', $this->package_native_root . 'cordova' ) );
        }

        // Step 9: Add GIT to the project
        if ( !is_dir( $this->package_native_root . '.git' ) ) {
            $this->flush( '# Adding git to the sencha project' );
            $this->rrmdir( "{$this->package_native_root}.git" );
            file_put_contents(
                "{$this->package_native_root}.gitignore",
                implode(
                    "\n",
                    array(
                        '# Ignore everything except for app web files',
                        '*',
                        '!app.js',
                        '!app.json',
                        '!app',
                        '!app/*',
                        '!app/*/*',
                        '!index.html',
                        '!resources',
                        'resources/*',
                        '!resrouces/css',
                        '!resrouces/css/*'
                    )
                )
            );
            $this->exec( sprintf( 'cd %s && git init && git add . && git commit -am \'[WP App Factory] initial auto commit\'', $this->package_native_root ) );
        } else {
            $this->flush( '# Committing changes to git' );
            $this->exec( sprintf( 'cd %s && git add . && git commit -am "[WP App Factory] committing build %d"', $this->package_native_root, $settings['build_number'] ) );
        }

        $url = get_permalink( $this->post->ID );
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            $this->flush( sprintf( "# App can now be viewed at %s", $url ) );
        } else {
            $this->flush( sprintf( "# App can now be viewed at <a href=\"%s\" target=\"_blank\">%s</a>", $url, $url ) );
        }

        $this->flush( "" );
        $this->flush( "# Next steps:" );
        $this->flush( "" );
        $this->flush(          "FOR DEVELOPMENT" );
        $this->flush(          "---------------" );
        $this->flush( sprintf( "    1) Download the Sencha Touch project ( try running `wp app download %s > /path/to/%s.zip` )", $this->post->post_name, $this->post->post_name ) );
        $this->flush(          "    2) Unzip the project zip file and change to the project rood directory" );
        $this->flush(          "    3) Add in the Hot Code Push development add on ( cd cordova && cordova plugin add cordova-hot-code-push-local-dev-addon && cd .. )" );
        $this->flush(          "    4) In another terminal, start the cordova-hcp server - ( cd cordova && cordova-hcp server )" );
        $this->flush(          "    5) Back in your original terminal, build the app ( sencha app build -dev )" );
        $this->flush(          "    6) Run the app in the simulator or device ( cordova run OR cordova run ios OR cordova run android )" );
        $this->flush(          "    7) Do your development in the project root directory.  Change files in the app/ directory.  Anytime you want to trigger a Hot Code push, rebuild the app ( sencha app build -dev )" );
    }

    /**
     * This is the listener for wp_ajax_download_sencha_package.  It instantiates the app factory based on $_REQUEST['slug']
     * ( which is the slug for a particular app ).  Once instantiated, it just calls $this->deliver_download.
     *
     * @return void
     */
    public function prepare_download()
    {
        global $post;
        $post = apply_filters( 'the_app_factory_sencha_get_post', get_page_by_path( $_REQUEST['slug'], OBJECT, APP_POST_TYPE ) );

        $the_app = &TheAppFactory::instantiateFromPost( $post, __CLASS__ );

        if ( $the_app ) {
            $the_app->deliver_download();
            exit;
        }
    }

    /**
     * Puts the contents of $string into the file at $dest
     *
     * @param string $string the string that you want to write to the file at $dest
     * @param string $dest the destination.  This string should be relative to the root ( i.e. index.html, not /Users/you/Sites/site/wp-content/uploads/wp-app-factory/build/sencha/ )
     *
     * @return void
     */
    public function build_put_string( $string, $dest )
    {
        $this->build_mkdir( dirname( $this->package_native_root . $dest ) );
        file_put_contents( $this->package_native_root . $dest, $string );
    }

    /**
     * Attempts to mkdir (deep) the $target directory
     *
     * @return void
     */
    public function build_mkdir($target){
        wp_mkdir_p( $target );
    }

    /**
     * Fetches (by cURL) the $url and copies it to the $dest
     *
     * ( $deprecated used to be $minify )
     *
     * @return void
     */
    public function build_cp($url,$dest,$deprecated = false, $silent = false){
        $fp = fopen($dest,'w');
        $content = $this->get_by_curl($url);

        if (!$silent){
            $this->flush( "[COPY] ".basename($url) );
        }
        fwrite($fp,$content);
        fclose($fp);
    }

    /**
     * Gets a URL by curl and returts the result
     *
     * @return $result result of curl_exec() - should be the contents of the url
     */
    public function get_by_curl($url,$ch=null,$url_parms = 'building=true'){
        if (isset($ch)){
            $_ch = & $ch;
        }
        else{
            $_ch = curl_init();
        }
        curl_setopt($_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($_ch, CURLOPT_HEADER, false);
        curl_setopt($_ch, CURLOPT_FOLLOWLOCATION, true);
        $sep = (strpos($url,'?') === false ? '?' : '&');
        curl_setopt($_ch, CURLOPT_URL, $url.$sep.$url_parms);

        // Access this hook if you need to add anything like user authentication
        do_action_ref_array('the_app_factory_get_by_curl',array(&$_ch));

        $result = curl_exec($_ch);

        if (!isset($ch)){
            curl_close($_ch);
        }
        return $result;
    }

    public static function package_image( $atts=array(), $content=null, $code='' ){
        $the_app = the_app();
        $image_url = $content;
        if ( $the_app->is_packaging && !empty( $image_url ) ) {
            $dir = isset( $atts['dir'] ) ? trailingslashit( $atts['dir'] ) : 'resources/images/';
            $relative_dest = $dir . basename( $image_url );
            if ( strpos( $relative_dest, '?' ) !== false ){
                $relative_dest = $dir . md5( basename( $image_url ) );
            }
            $dest = $the_app->package_native_root . $relative_dest;

            // Any images that need to be copied over we're going to queue up so that they don't copy and then get
            // clobbered when refreshing the app directory
            if ( !isset( $the_app->image_queue ) ) {
                $the_app->image_queue = array();
            }
            // Have to have these extra steps because I can't just do $the_app->image_queue['foo'] = 'bar' unless I
            // implement ArrayAccess for the class, and I'm not ready to do that right now.  @TODO
            $queue = $the_app->image_queue;
            $queue[ $image_url ] = $dest;
            $the_app->image_queue = $queue;
            return $relative_dest;
        }
        return $image_url;
    }


    // Thanks http://php.net/manual/en/function.rmdir.php
    // Recursively remove a directory
    public function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }


    /**
     * Generate a version of the index.html that will work with the Sencha packaged approach and put it in the Sencha
     * app directory
     *
     * @return void
     */
    function generateIndex()
    {
        ob_start();
        include APP_FACTORY_PATH . '/resources/templates/sencha/index.html.php';
        $this->build_put_string( ob_get_clean(), 'index.html' );
    }

    /**
     * Generate a version of the app.js file that will work with the Sencha packaged approach and put it in the Sencha
     * app directory
     *
     * @return void
     */
    function generateAppJs()
    {
        ob_start();
        include APP_FACTORY_PATH . 'resources/templates/sencha/app.js.php';
        $this->build_put_string( ob_get_clean(), 'app.js' );
    }

    public function generateAppJson()
    {
        if ( !file_exists( $this->package_native_root . 'app.clean.json' ) ) {
            // Because this step relieson the app.json file being in a particular format, and we're also overwriting that
            // file, I'm going to save off a clean/original copy of it, just in case.
            copy( $this->package_native_root . 'app.json', $this->package_native_root . 'app.clean.json' );
        }
        $json_string = file_get_contents( $this->package_native_root . 'app.clean.json' );
        $json_string = preg_replace_callback( '#(\n     \*/)(.+)(\n    /\*|\n\})#sU', array( __CLASS__, 'appJsonReplace' ), $json_string );

        $app_meta = the_app_get_app_meta( $this->post->ID );
        $name = $this->package_name;
        $app_identifier = $app_meta['bundle_id'] . '.' . $name;
        $extras = apply_filters(
            'the_app_factory_sencha_app_json_extras',
            // Gives a deep object
            json_decode( // 2: when I decode it, all associative arrays are now objects
                json_encode( // 1: First, json encode it, so that...
                    array(
                        'builds' => array(
                            'cordova' => array(
                                'packager' => 'cordova',
                                'cordova' => array(
                                    'config' => array(
                                        'platforms' => 'browser', // 'brower' is a safe platform as it does not require any additional SDKs to be installed
                                        'id' => $app_identifier
                                    )
                                )
                            ),
                            'ios' => array(
                                'packager' => 'cordova',
                                'cordova' => array(
                                    'config' => array(
                                        'platforms' => 'ios',
                                        'id' => $app_identifier
                                    )
                                )
                            ),
                            'android' => array(
                                'packager' => 'cordova',
                                'cordova' => array(
                                    'config' => array(
                                        'platforms' => 'android',
                                        'id' => $app_identifier
                                    )
                                )
                            ),
                        )
                    )
                )
            )
        );
        $json_string = preg_replace( '#,([^,]+)$#', ',' . "\n" . trim( json_encode( $extras, JSON_PRETTY_PRINT ), '{}' ) . '$1', $json_string );
        $this->build_put_string( $json_string, 'app.json' );
    }

    public function appJsonReplace( $match )
    {
        $subject = rtrim( trim( $match[2] ), ',' );
        $subject = preg_replace( '#/\*.*\*/#smU', '', $subject ); // strip remaining comments
        $object = (array)json_decode( '{' . $subject . '}' );
        if ( !$object ) {
            // Stupid, but sencha has the string '\.svn$' in the "ignore" member, and that doesn't parse.  It needs an extra \. Grrrrr.
            $subject = str_replace( '\\', '\\\\', $subject );
            $object = (array)json_decode( '{' . $subject . '}' );
        }
        if ( !$object ) {
            // If we still don't have anything, then just create something with the right key and a blank array for the value
            // @todo need to future proof this a little better -> who knows how something might fail in the future.  But
            // it would be good to get the actual structure
            preg_match( '#"([^"]+)"#', $subject, $matches );
            $object = array( $matches[1] => array() );
        }

        foreach ( $object as $key => & $value ) {
            $this->adjustAppJsonValue( $key, $value );
        }
        unset( $value ); // unsetting a reference just removes the reference

        do_action_ref_array( 'the_app_factory_sencha_app_json', array( & $object ) );

        $output = json_encode( $object, JSON_PRETTY_PRINT ); // create the JSON string
        $output = trim( $output, '{}' ); // trim starting and trailing curly braces
        $output = rtrim( $output, "\n" ) . ",\n"; // trim trailing line break and add a comma and linebreak
        $output = $match[1] . $output . $match[3]; // re-introduce the comments
        return $output;
    }

    /**
     * Take the App JSON value indexed by $key and ( maybe ) adjust it based on what is in our own JSON
     *
     * @param string $key the key
     * @param mixed $value the value ( could be an array, or a string, or an object, depending on the key.  Note, $value
     *                      is passed by reference and could change
     *
     * @return void
     */
    public function adjustAppJsonValue( $key, & $value )
    {
        $app_json = $this->app_json;
        if ( empty( $app_json ) ) {
            ob_start();
            include APP_FACTORY_PATH . 'resources/templates/sencha/app.json.php';
            $app_json = json_decode( ob_get_clean() );
            $this->app_json = $app_json ;
        }

        switch ( $key ) {
        case 'id':
            // Create a UUID formatted id from the md5 hash ( they actually map quite nicely )
            $value = implode(
                '-',
                array(
                    substr( $app_json->id, 0, 8 ),
                    substr( $app_json->id, 8, 4 ),
                    substr( $app_json->id, 12, 4 ),
                    substr( $app_json->id, 16, 4 ),
                    substr( $app_json->id, 20, 12 ),
                )
            );
            break;
        case 'css':
        case 'js':
            foreach ( $app_json->$key as $resource ) {
                switch ( true ) {
                case substr( $resource->path, 0, 3 ) == 'sdk':
                case $resource->path == 'app.js': // handled elsewhere
                    // This is the SDK resource, which is supplied now by the Sencha project.  Ignore
                    break;
                case strpos( $resource->path, plugins_url() ) !== false:
                    // It's a url within the plugins directory, copy the file
                    $path = str_replace( plugins_url(), WP_PLUGIN_DIR . '/', $resource->path );
                    $resource->path = "resources/$key/" . basename( $path );
                    $this->build_put_string( file_get_contents( $path ), $resource->path );
                    if ( isset( $resource->remote ) ) {
                        unset( $resource->remote );
                    }
                    $resource->update = 'delta';
                    $value[] = $resource;
                    break;
                default:
                    // It's a WP App Factory file, so copy it over directly
                    $this->build_put_string( file_get_contents( APP_FACTORY_PATH . $resource->path ), $resource->path );
                    $value[] = $resource;
                    break;
                }

            }
            break;
        case 'resources':
            $value = $app_json->resources;
            break;
        }

    }

    /**
     * Find out whatever $whats are queued for the app and copy over the appropriate file.  Note that no factory is
     * need to make these; they are straight Javascript files
     *
     * @return void
     */
    private function copyQueued()
    {
        foreach ( $this->queued as $what => $queue ) {
            if ( $what == 'require' ) {
                // The require queue gets handled just a little differently as it might refer to framework classes
                // the just need to be include, but that WP App Factory otherwise knows nothing about
                foreach ( $queue as $class ) {
                    if ( isset( $this->registered['paths']['path'][$class] ) ) {
                        $path = $this->registered['paths']['path'][$class];
                        if ( ( $this->isWindows() && substr( $path, 1, 1 ) != ':' )
                            || ( !$this->isWindows() && substr( $path, 0, 1 ) != '/' ) ) {
                            // If it doesn't start with '/', then it is relative to WP App Factory
                            $path = APP_FACTORY_PATH . $path;
                        }
                        // Need to dynamically figure out what category ( i.e. store, helper, proxy, controller, etc )
                        // this $class is.  We take that from the registered path, which is we take to be of the form /path/to/CATEGORY/filename.js
                        $category = basename( dirname( $path ) );
                        $this->flush( "# copying required $class" );
                        $this->build_put_string( file_get_contents( $path ), "app/$category/" . basename( $path ) );
                    }
                }
            } else {
                $this->flush( "# copying queued $what" );
                foreach ( $queue as $class ) {
                    if ( isset( $this->registered['paths'][$what][$class] ) ) {
                        $path = $this->registered['paths'][$what][$class];
                    } else {
                        $path = APP_FACTORY_PATH . "app/$what/$class.js";
                    }
                    $this->flush( "# copying queued $what - $class" );
                    $this->build_put_string( file_get_contents( $path ), "app/$what/$class.js" );
                }
            }
        }
    }

    /**
     * Copy over required javascript files.  This is one of the more important methods that we use and I need to document
     * out good use cases for exactly how to register and enqueue new files
     *
     * @return void
     */
    public function copyRequiredJsFiles()
    {
        $mothership = $this->mothership;

        // First, copy over whatever dynamic classes might exist - things that need to be run through a factory.js.php script
        foreach ( array( 'model', 'store', 'helper' ) as $what ) {
            foreach ( $this->__get( "{$what}s" ) as $class => $definition ) {
                $this->flush( "# generating $what/$class" );
                set_query_var( APP_APP_VAR, "$class.js" );
                set_query_var( 'NO_CONTENT_TYPE_HEADER', true );
                ob_start();
                $save_what = $what;
                include APP_FACTORY_PATH . "app/$what/factory.js.php";
                $what = $save_what;
                $this->build_put_string( ob_get_clean(), "app/$what/$class.js" );

            }
        }

        // Now, copy over whatever classes have been queued up
        $this->copyQueued();
    }

    /**
     * Calls to $this->package_image will queue up images.  We actually deploy them in this step to avoid them getting
     * clobbered when removing the deployed sencha directory ( on a clean build ).  This is an issue because the post
     * gets parsed before running the prepare_sencha() method, where the very first thing it might do is blow away the
     * existing directory
     *
     * @param bool $dry_run if true, then it doesn't actually copy the files
     *
     * @return void
     */
    public function deploy_queued_images( $dry_run = null )
    {
        if ( !isset( $dry_run ) ) {
            $dry_run = !$this->clean_build;
        }
        if ( !$dry_run ) {
            $this->flush( '# Deploying Queued Images' );
            foreach ( $this->image_queue as $image_url => $dest ) {
                $this->build_mkdir( dirname( $dest ) );
                $this->build_cp( $image_url, $dest );
            }
        }
    }

    /**
     * So much talk about sizes for icon images.  I am going to start taking what is at
     * https://github.com/phonegap/phonegap/wiki/App-Icon-Sizes as bible.  Today it says:
     * iOS
     *      57px iPhone
     *      72px iPad
     *      114px iPhone 4 Retina Display
     *      144px iPad 3 Retina Display
     *      1024px iTunes – Used in iTunes and in the App Store sized down to 175px
     *      29px iPhone Settings/Spotlight, iPad Settings – used in these table views. Minor, but still important!
     *      48px iPad Spotlight
     *      58px iPhone 4 Settings/Spotlight
     *      64px document icon
     *      320px document icon
     *
     * More Info
     *      [Apple Guidelines](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/index.html)
     *      [iOS App Icon Sizes](http://mrgan.tumblr.com/post/708404794/ios-app-icon-sizes)
     *
     * Android
     *      192px (xxxhdpi)
     *      144px (xxhdpi)
     *      96px (xhdpi)
     *      72px (hdpi)
     *      48px (mdpi)
     *      36px (ldpi)
     *      512px (store)pixel - only used in Android Market; resized to various sizes
     *
     * More Info
     *      [Android Guidelines](https://www.google.com/design/spec/style/icons.html)
     *
     * @param string $url a url of an icon.  Get it into the app by attaching a 512x512 or 1024x1024 image called
     *                        icon.png to the app post
     * @param bool $dry_run if true, then it doesn't actually copy the images, but it does set $the_app->icon_sizes
     *
     * @return void
     */
    public function deploy_icons( $url, $dry_run = null )
    {
        if ( !isset( $dry_run ) ) {
            $dry_run = !$this->clean_build;
        }

        $this->apply(
            'icon_sizes',
            $looper = array(
                'ios' => array(
                    '40' => '40.png',
                    '80' => '80.png',
                    '50' => '50.png',
                    '100' => '100.png',
                    '60' => '60.png',
                    '120' => '120.png',
                    '180' => '180.png',
                    '72' => '72.png',
                    '144' => '144.png',
                    '76' => '76.png',
                    '152' => '152.png',
                    '29' => '29.png',
                    '58' => '58.png',
                    '57' => '57.png',
                    '114' => '114.png',
                    '1024' => '1024.png',
                ),
                'android' => array(
                    '192' => 'xxxhdpi.png',
                    '144' => 'xxhdpi.png',
                    '96' => 'xhdpi.png',
                    '72' => 'hdpi.png',
                    '48' => 'mdpi.png',
                    '36' => 'ldpi.png',
                    '512' => 'store.png'
                )
            )
        );

        if ( $dry_run === false ) {
            $this->flush( '# Deploying icons' );
            foreach ( $looper as $target => $sizes ) {
                $this->create_images( $url, "resources/icons/$target", $sizes );
            }
        }
    }

    public function create_images( $url, $path, $sizes ){
        // Returns an object with:
        //	->image = image resource
        //  ->size = the result from getimagesize()
        $image = $this->curl_image( $url );

        $the_app = $this;

        if ($image){
            foreach ($sizes as $pixels => $filename){
                if ( strpos( $pixels, 'x' ) ){
                    list($width,$height) = explode('x',$pixels);
                    $type = 'splash';
                }
                else{
                    $type = 'icon';
                    $width = $height = $pixels;
                }

                $dest = imagecreatetruecolor( $width, $height );
                imagealphablending($dest, false);
                $color = imagecolortransparent($dest, imagecolorallocatealpha($dest, 0, 0, 0, 127));
                imagefill($dest, 0, 0, $color);
                imagesavealpha($dest, true);

                $result = imagecopyresampled( $dest, $image->image, 0, 0, 0, 0, $width, $height, $image->width, $image->height );

                if (is_array($filename)){
                    foreach ($filename as $name){
                        $this->build_mkdir( dirname($this->get('package_native_root') . "$path/$name") );
                        imagepng( $dest, $this->get('package_native_root') . "$path/$name" );
                        $this->flush( "[CREATED] $name" );
                    }
                }
                else{
                    $this->build_mkdir( dirname($this->get('package_native_root') . "$path/$filename") );
                    imagepng( $dest, $this->get('package_native_root') . "$path/$filename" );
                    $this->flush( "[CREATED] $filename" );
                }

                imagedestroy( $dest );
            }
        }
    }

    public function curl_image( $url ){
        $ch = curl_init(str_replace(array(' '),array('%20'),$url));
        $filename = tempnam( sys_get_temp_dir(), 'afoy_image_');
        $fp = fopen($filename,'wb');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $result = curl_exec($ch);
        $mime = curl_getinfo($ch,CURLINFO_CONTENT_TYPE);
        fclose($fp);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($result and $code != 404){
            switch($mime){
            case 'image/png':
                $image = imagecreatefrompng( $filename );
                //imagealphablending($image, true);
                //imagesavealpha($image, true);
                break;
            case 'image/jpeg':
                $image = imagecreatefromjpeg( $filename );
                break;
            case 'image/gif':
                $image = imagecreatefromgif( $filename );
                break;
            }

            $size = getimagesize( $filename );

            @unlink( $filename );
            return (object)array(
                'image' => $image,
                'width' => $size[0],
                'height' => $size[1]
            );
        }
        @unlink($path . $filename);
        return false;
    }

    /**
     * So much talk about sizes for startup images.  I am going to start taking what is at
     * https://github.com/phonegap/phonegap/wiki/App-Splash-Screen-Sizes as bible.  Today it says
     *
     * Android:
     *      LDPI:
     *          Portrait: 200x320px
     *          Landscape: 320x200px
     *      MDPI:
     *          Portrait: 320x480px
     *          Landscape: 480x320px
     *      HDPI:
     *          Portrait: 480x800px
     *          Landscape: 800x480px
     *      XHDPI:
     *          Portrait: 720px1280px
     *          Landscape: 1280x720px
     *      XXHDPI:
     *          Portrait: 960px1600px
     *          Landscape: 1600x960px
     *      XXXHDPI:
     *          Portrait: 1280px1920px
     *          Landscape: 1920x1280px
     *
     * iOS
     *      Tablet (iPad)
     *      Non-Retina (1x)
     *          Portrait: 768x1024px
     *          Landscape: 1024x768px
     *      Retina (2x)
     *          Portrait: 1536x2048px
     *          Landscape: 2048x1536px
     *      Handheld (iPhone, iPod)
     *      Non-Retina (1x)
     *          Portrait: 320x480px
     *          Landscape: 480x320px
     *      Retina (2x)
     *          Portrait: 640x960px
     *          Landscape: 960x640px
     *      iPhone 5 Retina (2x)
     *          Portrait: 640x1136px
     *          Landscape: 1136x640px
     *      iPhone 6 (2x)
     *          Portrait: 750x1334px
     *          Landscape: 1334x750px
     *      iPhone 6 Plus (3x)
     *          Portrait: 1242x2208px
     *          Landscape: 2208x1242px
     *
     * Notes
     * According to Apple's application guidelines, a tablet (iPad) application should not hide the status bar while
     * a handheld (iPhone) application should hide status bar.
     *
     * The above dimensions are assuming that you have chosen to follow this recommendation. If you choose a different
     * path, then you should increase or decrease the image height based on the size of the status bar.
     *
     * An iPhone app should include one launch image in portrait orientation; an iPad app should include one launch
     * image in portrait orientation and one launch image in landscape orientation. This means that not all formats
     * listed above are required.
     *
     * Get these files by attaching the following images to the app post:an image called startup-portrait.png and startup-landscape.png
     *
     *      startup-portrait.png  ( recommended size 1536 x 2048 )
     *      startup-landscape.png ( recommended size 2048 x 1536 )
     *
     * @param string $portrait - url to the portrait image
     * @param string $landscape - url to the portrait image
     * @param bool $dry_run if true, then it doesn't actually copy the images, but it does set $the_app->icon_sizes
     *
     * @return void
     */
    public function deploy_startup_images( $portrait, $landscape, $dry_run = null )
    {
        if ( !isset( $dry_run ) ) {
            $dry_run = !$this->clean_build;
        }

        $this->apply(
            'startup_sizes',
            $looper = array(
                'ios' => array(
                    'portrait' => array(
                        '640x1136' => '640x1136.png',
                        '750x1334' => '750x1334.png',
                        '768x1024' => '768x1024.png',
                        '1536x2048' => '1536x2048.png',
                        '320x480' => '320x480.png',
                        '640x960' => '640x960.png',
                        '1242x2208' => '1242x2208.png',
                    ),
                    'landscape' => array(
                        '2208x1242' => '2208x1242.png',
                        '2048x1536' => '2048x1536.png',
                        '1024x768' => '1024x768.png',
                        '480x320' => '480x320.png',
                        '960x640' => '960x640.png',
                        '1136x640' => '1136x640.png',
                        '1334x750' => '1334x750.png',
                    )
                ),
                'android' => array(
                    'portrait' => array(
                        '200x320' => 'port-ldpi.png',
                        '320x480' => 'port-mdpi.png',
                        '480x800' => 'port-hdpi.png',
                        '720x1280' => 'port-xhdpi.png',
                        '960x1600' => 'port-xxhdpi.png',
                        '1280x1920' => 'port-xxxhdpi.png',
                    ),
                    'landscape' => array(
                        '320x200' => 'land-ldpi.png',
                        '480x320' => 'land-mdpi.png',
                        '800x480' => 'land-hdpi.png',
                        '1280x720' => 'land-xhdpi.png',
                        '1600x960' => 'land-xxhdpi.png',
                        '1920x1280' => 'land-xxxhdpi.png',
                    )
                )
            )
        );

        if ( $dry_run === false ) {
            $this->flush( '# Deploying Startup Images' );
            foreach ( $looper as $target => $orientations ) {
                foreach ( $orientations as $orientation => $sizes ) {
                    $this->create_images( $$orientation, "resources/startup/$target", $sizes );
                }
            }
        }
    }

    public function deploy_startup_phone( $url, $dry_run = null )
    {
        if ( !isset( $dry_run ) ) {
            $dry_run = !$this->clean_build;
        }

        $this->apply(
            'startup_sizes',
            $looper = array(
                'ios' => array(
                    'portrait' => array(
                        '320x480' => '320x480.png',
                        '640x960' => '640x960.png',
                        '640x1136' => '640x1136.png',
                        '750x1334' => '750x1334.png',
                    ),
                ),
                'android' => array(
                    'portrait' => array(
                        '200x320' => 'port-ldpi.png',
                        '320x480' => 'port-mdpi.png',
                        '480x800' => 'port-hdpi.png',
                        '720x1280' => 'port-xhdpi.png',
                    ),
                )
            )
        );

        if ( $dry_run === false ) {
            $this->flush( '# Deploying Phone Startup Image' );
            foreach ( $looper as $target => $orientations ) {
                foreach ( $orientations as $orientation => $sizes ) {
                    $this->create_images( $url, "resources/startup/$target", $sizes );
                }
            }
        }
    }

    public function deploy_startup_tablet( $url, $dry_run = null )
    {
        if ( !isset( $dry_run ) ) {
            $dry_run = !$this->clean_build;
        }

        $this->apply(
            'startup_sizes',
            $looper = array(
                'ios' => array(
                    'portrait' => array(
                        '768x1024' => '768x1024.png',
                        '1536x2048' => '1536x2048.png',
                        '1242x2208' => '1242x2208.png',
                    ),
                ),
                'android' => array(
                    'portrait' => array(
                        '960x1600' => 'port-xxhdpi.png',
                        '1280x1920' => 'port-xxxhdpi.png',
                    ),
                )
            )
        );

        if ( $dry_run === false ) {
            $this->flush( '# Deploying Tablet Startup Image' );
            foreach ( $looper as $target => $orientations ) {
                foreach ( $orientations as $orientation => $sizes ) {
                    $this->create_images( $url, "resources/startup/$target", $sizes );
                }
            }
        }
    }

    public function deploy_startup_landscape_tablet( $url, $dry_run = null )
    {
        if ( !isset( $dry_run ) ) {
            $dry_run = !$this->clean_build;
        }

        $this->apply(
            'startup_sizes',
            $looper = array(
                'ios' => array(
                    'landscape' => array(
                        '1024x768' => '1024x768.png',
                        '2048x1536' => '2048x1536.png',
                        '2208x1242' => '2208x1242.png',
                    )
                ),
                'android' => array(
                    'landscape' => array(
                        '320x200' => 'land-ldpi.png',
                        '480x320' => 'land-mdpi.png',
                        '800x480' => 'land-hdpi.png',
                        '1280x720' => 'land-xhdpi.png',
                        '1600x960' => 'land-xxhdpi.png',
                        '1920x1280' => 'land-xxxhdpi.png',
                    )
                )
            )
        );

        if ( $dry_run === false ) {
            $this->flush( '# Deploying Landscape Tablet Startup Image' );
            foreach ( $looper as $target => $orientations ) {
                foreach ( $orientations as $orientation => $sizes ) {
                    $this->create_images( $url, "resources/startup/$target", $sizes );
                }
            }
        }
    }

    /**
     * Checks to see if we're using the manifest, which means we want to store things offline on the device, and if so
     * then deploy the data as need be.
     *
     * @return void
     */
    public function deployData()
    {
        if ( $this->is_using_manifest ) {
            // They are using the manifest, meaning they want the data to be available
            // offline.  So, we're going to get all of the initial data and store it
            // with the packaged app.
            $this->build_mkdir( $this->package_native_root . 'resources/data' );
            foreach ( $this->stores as $store ) {
                if ( isset( $store['proxy'] ) and isset( $store['useLocalStorage'] ) && $store['useLocalStorage'] ) {
                    if ( preg_match( '#' . APP_POST_TYPE . '/' . $this->post->post_name . '/data/(.*)$#', $store['proxy']['url'], $matches ) ) {
                        set_query_var( 'NO_CONTENT_TYPE_HEADER', true );
                        $this->build_put_string( $this->getData( $matches[1] ), "resources/data/{$store['model']}.json" );
                    } else {
                        $this->build_cp( $store['proxy']['url'], $this->package_native_root . "resources/data/{$store['model']}.json" );
                    }
                }
            }
        }
    }

    /**
     * A helper method to get the data for the supplied basename.  This triggers if the url for the data is
     * http://domain.com/apps/my-app-slug/data/foo.  $basename is set to basename( $url );
     *
     * @param string $basename the name of the data variable we are retrieiving
     *
     * @return string the JSON, possibly wrapped in JSONP callback
     */
    public function getData( $basename )
    {
        set_query_var( APP_DATA_VAR, $basename );
        $the_app = $this;
        ob_start();
        include APP_FACTORY_PATH . 'app/data/factory.js.php';
        return ob_get_clean();
    }

    /**
     * Retrieves the appropriate file ( either chcp.json or chcp.manifest from a built app.
     *
     * @param string $endpoint the file ( chcp.json or chcp.manifest ) that is being requested
     *
     * @return
     */
    public function getHotCodePushData( $endpoint )
    {
        header( 'Content-type: application/json' );
        ob_start();
        if ( file_exists( $file = "{$this->archive_root}www/$endpoint" ) ) {
            echo file_get_contents( $file );
        } elseif ( file_exists( $file = "{$this->package_native_root}cordova/www/$endpoint" ) ) {
            echo file_get_contents( $file );
        }
        return ob_get_clean();
    }

    public function adjustConfigXML()
    {
        $config_file = $this->package_native_root . 'cordova/config.xml';

        if ( file_exists( $config_file ) ) {
            $clean_config_file = $this->package_native_root . 'cordova/config.clean.xml';
            if ( !file_exists( $clean_config_file ) ) {
                // Let's make a backup of the original config.xml file, just in case, and we'll use that
                copy( $config_file, $clean_config_file );
            }

            // The str_replace is necessary to make $config_xml->xpath work
            $config_xml = simplexml_load_string( str_replace( 'xmlns=', 'ns=', file_get_contents( $clean_config_file ) ) );
            if ( empty( $config_xml ) ) {
                return;
            }

            $settings = $this->getSettings();
            $config_xml['version'] = $settings['version'];
            $config_xml['android-versionCode'] = $this->getVersionCode() / 10;  // See comments at "min_native_interface" on https://github.com/nordnet/cordova-hot-code-push for why we divide by 10
            $config_xml['ios-CFBundleVersion'] = $this->getVersionCode();
            $author = $config_xml->xpath( "//author" )[0];
            $author['email'] = $settings['author_email'];
            $author['href'] = $settings['author_url'];
            $author[0] = $settings['author_name'];
            $description = $config_xml->xpath( "//description" )[0];
            $description[0] = $settings['description'];

            // Let's add in the icons & startups
            if ( $icons = $this->icon_sizes ) {
                foreach ( $icons as $platform => $sizes ) {
                    $element = $config_xml->xpath( "//platform[@name=\"$platform\"]" )[0];
                    if ( empty( $element ) ) {
                        continue;
                    }
                    foreach ( $sizes as $size => $filename ) {
                        $icon = $element->addChild( 'icon' );
                        $icon->addAttribute( 'src', "../resources/icons/$platform/$filename" );
                        switch ( $platform ) {
                        case 'ios':
                            $icon->addAttribute( 'width', $size );
                            $icon->addAttribute( 'height', $size );
                            break;
                        case 'android':
                            $icon->addAttribute( 'density', basename( $filename, '.png' ) );
                            break;
                        }
                    }
                }
            }

            // Let's add in the startups
            if ( $startups = $this->startup_sizes ) {
                foreach ( $startups as $platform => $orientations ) {
                    $element = $config_xml->xpath( "//platform[@name=\"$platform\"]" )[0];
                    if ( empty( $element ) ) {
                        continue;
                    }
                    foreach ( $orientations as $orientation => $sizes ) {
                        foreach ( $sizes as $size => $filename ) {
                            $splash = $element->addChild( 'splash' );
                            $splash->addAttribute( 'src', "../resources/startup/$platform/$filename" );
                            switch ( $platform ) {
                            case 'ios':
                                list( $width, $height ) = explode( 'x', $size );
                                $splash->addAttribute( 'width', $width );
                                $splash->addAttribute( 'height', $height );
                                break;
                            case 'android':
                                $splash->addAttribute( 'density', basename( $filename, '.png' ) );
                                break;
                            }
                        }
                    }
                }
            }

            /*
             * This is important.  If we're using the hot code push plugin, which we normally do, then we're going to
             * adjust the config.xml file to have the following:
             *
             * <chcp>
             *  <auto-install enabled="false"/>
             *  <auto-download enabled="false"/>
             *  <config-file url="http://domain.com/apps/app-slug/chcp.json"/>
             * </chcp>
             *
             * The important thing to know is that we are specifically setting the config-file to the "virtual" file
             * chcp.json off of the apps/app-slug url.  When requested, this url will redirect to the chcp.json file
             * in either the build or archive directories.  Those files will contain information about the current
             * manifest, versions and identifiers.
             *
             * This means that when doing local development, if you want to test the hot code push plugin functionality,
             * then you'll have to get rid of the <chcp></chcp> element in config.xml ( maybe rename to <chcp-tmp/> ? )
             */
            if ( $this->is_using_hot_code_push ) {
                // Add in the Cordova Hot Code Push settings
                $chcp = $config_xml->addChild( 'chcp' );
                $autoInstall = $chcp->addChild( 'auto-install' );
                $autoInstall->addAttribute( 'enabled', 'false' );
                $autoDownload = $chcp->addChild( 'auto-download' );
                $autoDownload->addAttribute( 'enabled', 'false' );
                $configFile = $chcp->addChild( 'config-file' );
                $configFile->addAttribute( 'url', apply_filters( 'the_app_factory_hcp_config_file_url', get_permalink( $this->post->ID ) . 'chcp.json' ) );
            }

            // <preference name="BackupWebStorage" value="local"/>
            $preference = $config_xml->addChild( 'preference' );
            $preference->addAttribute( 'name', 'BackupWebStorage' );
            $preference->addAttribute( 'value', 'local' );

            do_action_ref_array( 'the_app_factory_sencha_config_xml', array( & $config_xml ) );

            $dom = new DOMDocument( "1.0" );
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML( $config_xml->asXML() );
            file_put_contents( $config_file, str_replace( ' ns=', ' xmlns=', $dom->saveXML() ) );
        }
    }

    public function addPlugin( $plugin )
    {
        $cordova_directory = $this->package_native_root . 'cordova' . DIRECTORY_SEPARATOR;
        $this->flush( '# Adding Cordova Plugin ' . $plugin );
        if ( $this->isWindows() ) {
            $this->passthru( sprintf( 'cd %s && cordova plugin add %s', $cordova_directory, $plugin ) );
        } else {
            $this->passthru( sprintf( '[ -d %s ] && cd %s && cordova plugin add %s 2>&1', $cordova_directory, $cordova_directory, $plugin ) );
        }
    }

    public function addPlatform( $platform )
    {
        $cordova_directory = $this->package_native_root . 'cordova' . DIRECTORY_SEPARATOR;
        $this->flush( '# Adding Cordova Platform ' . $platform );
        if ( $this->isWindows() ) {
            $this->passthru( sprintf( 'cd %s && cordova platform add %s', $cordova_directory, $platform ) );
        } else {
            $this->passthru( sprintf( '[ -d %s ] && cd %s && cordova platform add %s 2>&1', $cordova_directory, $cordova_directory, $platform ) );
        }
    }

    /**
     * Echo out the string and flush the output buffer
     *
     * @param string $string what to echo
     * @return void
     */
    public function flush( $string )
    {
        if ( defined( 'WP_CLI' ) && WP_CLI ) {
            WP_CLI::line( $string );
        } else {
            echo "$string\n";
            flush();
        }
    }

    /**
     * Deliver a zip file that can be downloaded.  The zip is a zip of the Sencha package directory that has been
     * built by WP App Factory
     *
     * @return void
     */
    public function deliver_download()
    {
        // Check if there is an environment already, if not, then build it.
        if ( !is_dir( $this->package_native_root ) ) {
            ob_start();
            $this->prepare_sencha_steps( true );
            ob_end_clean();
        }

        $filename = tempnam( sys_get_temp_dir(), 'afoy_zip_') . '.zip';
        $this->exec( sprintf( 'cd %s && zip -x \*app.clean.json \*config.clean.xml -r %s %s', dirname( $this->package_native_root ), $filename, basename( $this->package_native_root ) ) );
        if ( !defined( 'WP_CLI' ) ) {
            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $this->post->post_name . '.zip"');
        }
        readfile( $filename );
        @unlink( $filename );
        exit;
    }

    /**
     * For the current app post, get the version code, which is based off of the major version of the app concatenated
     * with the current build number ( from app meta ).  For example, if the current version of the app is 1.0.1, then
     * the major version is 1 and the version code will be '1' . $settings['build_number']
     *
     * I am directing a decision to multiply the build number by 10 based off of comments at
     * https://github.com/nordnet/cordova-hot-code-push ( search for min_native_interface )
     *
     * @return string
     */
    public function getVersionCode()
    {
        $settings = $this->getSettings();
        return $this->getMinimumNativeVersionCode() + $settings['build_number'] * 10;
    }

    /**
     * Returns the minimum native version code.  This is based off of the major version number of the app and is essentially
     * that number followed by 7 zeros.  When generating the Version Code ( @see getVersionCode() above ), then the
     * current build number ( from app meta ) is added to this number.  This is done so that a version code is derived
     * directly off of the major version and can be guaranteed to be greater than this minimumNativeVersionCode().
     *
     * @return int
     */
    public function getMinimumNativeVersionCode()
    {
        $settings = $this->getSettings();
        preg_match( '/^[0-9]+/', $settings['version'], $matches );
        return intval( $matches[0] ) * pow( 10, 7 );
    }

    /**
     * Takes a zip file and publishes it to the archive_root of the app.
     *
     * @param string $zip the filename of the zip file
     *
     * @return void
     */
    public function publish_from_zip( $zip )
    {
        $this->rrmdir( $this->archive_root );
        $this->build_mkdir( $this->archive_root );
        $this->passthru( sprintf( 'unzip %s -d %s', $zip, $this->archive_root ) );
        $this->passthru( sprintf( 'cd %s && cordova-hcp build', $this->archive_root ) );
        file_put_contents( "{$this->archive_root}www/chcp.json", json_encode( $this->get_chcp_json( $this->archive_root_url . 'www/' ), JSON_PRETTY_PRINT ) );
    }

    public function archive()
    {
        // Check if there is an environment already, if not, then build it.
        if ( !is_dir( $this->package_native_root ) ) {
            ob_start();
            $this->prepare_sencha_steps( true );
            ob_end_clean();
        }

        $filename = tempnam( sys_get_temp_dir(), 'afoy_zip_') . '.zip';

        // Add the www directory to the zip archive
        $this->exec( sprintf( 'cd %s && zip -r %s %s', $this->package_native_root . 'cordova', $filename, 'www' ) );

        $this->publish_from_zip( $filename );

        @unlink( $filename );

    }

    /**
     * Get an array representing what would go in the chcp.json file for the Cordova Hot Code Push Plugin
     *
     * @return array of settings.
     */
    public function get_chcp_json( $root_url = null )
    {
        if ( !isset( $root_url ) ) {
            $root_url = $this->package_native_root_url . 'cordova/www/';
        }
        $settings = $this->getSettings();
        $chcp = array(
            'content_url' => $root_url,
            'release' => date( 'Y.m.d-H.i.s'),
            'update' => 'start',
            'android_identifier' => $settings['bundle_id'] . '.' . $this->package_name,
            'min_native_interface' => $this->getMinimumNativeVersionCode(),
            'ios_identifier' => $settings['ios_identifier']
        );
        return apply_filters( 'the_app_factory_chcp_json', $chcp );
    }

}
