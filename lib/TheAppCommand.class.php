<?php

/**
 * Control various aspects of apps in WP App Factory.
 */
class TheAppCommand extends WP_CLI_Command
{
    /**
     * Build the app identified by the required slug.
     *
     * The slug is found in the URL of the app - http://domain.com/apps/this-is-the-slug.
     *
     * Running `wp app build hello-world` will first check if the Sencha environment
     * exists for that app, and if not will create it.  If it already exists, then the
     * build command will just regenerate and copy over the files from the build into
     * the Sencha environment.
     *
     * If you want to force a clean refresh of the Sencha environment, then run
     * `wp app build hello-world --clean`
     *
     * ## OPTIONS
     *
     * <slug>
     * : The slug of the app post to build
     *
     * [--clean]
     * : Recreate the Sencha environment before running the build
     *
     * ## EXAMPLES
     *
     *     wp app build hello-world
     *     wp app build hello-world --clean
     *
     * @synopsis <slug> [--clean]
     */
    function build( $args, $assoc_args ) {
        list( $slug ) = $args;
        extract( $assoc_args );

        $the_app = TheAppFactory::instantiateFromSlug( $slug, 'TheAppSenchaPackager' );

        if ( !$the_app->post ) {
            WP_CLI::error( "Could not find any app with the slug '$slug'" );
        }

        if ( !$the_app->isAvailable() ) {
            $defaults = $the_app->get_default_atts( 'the_app' );
            WP_CLI::error(
                implode(
                    "\n",
                    array(
                        sprintf( __( 'This option is not currently available.  Please make sure that when you shell into the server as %s that the `sencha` command is available.  If you need to add it to your path, please add it in .bash_profile.', 'app-factory' ), $the_app->exec( 'whoami' ) ),
                        sprintf( __( 'Additionally, you must have an environment variable exported called SENCHA_SDK_%s that is the full path to the Sencha Touch SDK for version %s on the server.  Again, .bash_profile is your friend.', 'app-factory' ), str_replace( '.', '', $defaults['sdk'] ), $defaults['sdk'] )
                    )
                )
            );
        }

        // Check if there is an environment already, if not, then force clean.
        if ( !is_dir( $the_app->package_native_root ) ) {
            $clean = true;
        }

        $the_app->prepare_sencha_steps( $clean ? 'prepare_sencha' : 'prepare_sencha_files' );
    }

    /**
     * Download as a zip the built Sencha package for the app identified by the required slug.
     *
     * If the app has not been packaged, then it runs the build on it, otherwise, it will just
     * deliver the directory as a zip file.
     *
     * What gets delivered is a Sencha project that can be worked on locally.
     *
     * ## OPTIONS
     *
     * <slug>
     * : The slug of the app post to build
     *
     * ## EXAMPLES
     *
     *     wp app download hello-world
     *
     * @synopsis <slug>
     *
     * @param array $args       the command line arguments passed
     * @param array $assoc_args any flags that were set
     *
     * @return void
     */
    function download( $args, $assoc_args )
    {
        list( $slug ) = $args;
        extract( $assoc_args );

        $the_app = TheAppFactory::instantiateFromSlug( $slug, 'TheAppSenchaPackager' );

        if ( !$the_app->post ) {
            WP_CLI::error( "Could not find any app with the slug '$slug'" );
        }

        $the_app->deliver_download();
    }

    /**
     * Archives the app as it is right now for consumption by the Hot Code Push plugin
     *
     * This creates a copy of just the cordova/www directory and builds
     * the chcp.json file and chcp.manifest file therein.  The directory is then
     * archived and available for a hot code push.
     *
     * ## OPTIONS
     *
     * <slug>
     * : The slug of the app post to archive
     *
     * [--clean]
     * : Recreate the Sencha environment before running the build
     *
     * ## EXAMPLES
     *
     *     wp app archive hello-world
     *
     * @synopsis <slug>
     *
     * @param array $args       the command line arguments passed
     * @param array $assoc_args any flags that were set
     *
     * @return void
     */
    function archive( $args, $assoc_args )
    {
        list( $slug ) = $args;
        extract( $assoc_args );

        $the_app = TheAppFactory::instantiateFromSlug( $slug, 'TheAppSenchaPackager' );

        if ( !$the_app->post ) {
            WP_CLI::error( "Could not find any app with the slug '$slug'" );
        }

        $the_app->archive();
    }

    /**
     * Get or set configuration variables for the app.
     *
     * Run `wp app config foo` to see a list of available variables that
     * can be set.  Then run `wp app config foo --variable=value` to set a variable.
     *
     * @param array $args       the command line arguments passed
     * @param array $assoc_args any flags that were set.  In this context, it would be settings variables we want to set
     *
     * @return void
     */
    function config( $args, $assoc_args )
    {
        list( $post_name ) = $args;

        $the_app = TheAppFactory::instantiateFromSlug( $post_name, 'TheAppSenchaPackager' );

        if ( !$the_app->post && !empty( $post_name ) ) {
            WP_CLI::error( "Could not find any app with the slug '$post_name'" );
        }

        $settings = $the_app->getSettings();

        if ( empty( $assoc_args ) ) {
            // Being used as a getter - get back all settings
            foreach ( TheAppMeta::$settings as $slug => $setting ) {
                echo "# {$setting['title']} ( $slug )\n";
                echo "# {$setting['description']}\n";
                if ( empty( $post_name ) ) {
                    echo "# Default: ";
                }
                echo "{$settings[$slug]}\n";
                echo "\n";
            }

            echo "\n";
        } else {
            foreach ( $assoc_args as $key => $value ) {
                if ( array_key_exists( $key, TheAppMeta::$settings ) ) {
                    $settings[$key] = $value;
                } else {
                    WP_CLI::error( sprintf( __( '%s is not a valid setting slug. Run `wp app config` to see what is allowed', 'app-factory' ), $key ) );
                }
            }
            update_post_meta( $the_app->post->ID, 'app_meta', $settings );
        }
    }

    /**
     * Clean out a build directory.
     *
     * @param array $args       the command line arguments passed
     * @param array $assoc_args any flags that were set
     *
     * @synopsis <slug>
     *
     * @return void
     */
    function clean( $args, $assoc_args )
    {
        list( $slug ) = $args;
        extract( $assoc_args );

        $the_app = TheAppFactory::instantiateFromSlug( $slug, 'TheAppSenchaPackager' );

        if ( !$the_app->post ) {
            WP_CLI::error( "Could not find any app with the slug '$slug'" );
        }

        $the_app->rrmdir( $the_app->package_native_root );

        if ( is_dir( $the_app->package_native_root ) ) {
            WP_CLI::error( sprintf( 'Could not clean %s', $slug ) );
        } else {
            WP_CLI::success( sprintf( 'Successfully cleaned %s', $slug ) );
        }
    }
}