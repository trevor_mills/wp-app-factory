<?php

/**
 * Class TheAppMeta
 *
 * This class handles validating and retrieving app_meta settings for the app.
 */
class TheAppMeta
{
    /**
     * The post id that this instance has been instantiated with
     *
     * @var integer
     */
    var $post_id;

    /**
     * Defines the available settings that the app can have and cares about.  The key is the setting slug and the value
     * is a definition of the setting.  Gets set in the self::standup() method.
     *
     * @var null|array
     */
    static $settings = null;

    /**
     * Used to setup the self::$settings static, if it hasn't been setup already.  i put it in this method so that I
     * could pass it through a filter.
     *
     * Every setting must have a title and description ( wrapped in __( '', 'app-factory' ) preferably ) ) as well
     * as a default ( which can be null )
     *
     * @return void
     */
    private static function standup()
    {
        if ( !isset( self::$settings ) ) {
            self::$settings = apply_filters(
                'the_app_factory_meta_settings',
                array(
                    'bundle_id' => array(
                        'title' => __( 'Bundle Identifier Prefix', 'app-factory' ),
                        'description' => __( 'The prefix ( usially reverse domain, i.e. com.domain ) that goes before the bundle identifier for this app.', 'app-factory' ),
                        'default' => ''
                    ),
                    'version' => array(
                        'title' => 'Version',
                        'description' => __( 'The version number to build for the app stores.  Note, the first number ( the "major" version ) is used to determine if an app store update is required for a Hot Code Push.  You should increment the major version only if you are adding in new Cordova plugins.  Anything that touches only the web resources can and should stay in the same major version.', 'app-factory' ),
                        'default' => '0.0.1'
                    ),
                    'build_number' => array(
                        'title' => 'Build Number',
                        'description' => __( 'The current build number.  This is set automatically and should not be manually changed.', 'app-factory' ),
                        'default' => 0,
                        'type' => 'private'
                    ),
                    'description' => array(
                        'title' => 'Description',
                        'description' => __( 'Doesn\'t appear anywhere other than the config.xml file, but fill it in if you want', 'app-factory' ),
                        'default' => '',
                        'type' => 'textarea'
                    ),
                    'author_name' => array(
                        'title' => 'Author Name',
                        'description' => __( 'Name of the lead developer on the app.', 'app-factory' ),
                        'default' => ''
                    ),
                    'author_email' => array(
                        'title' => 'Author Email',
                        'description' => __( 'Email of the lead developer on the app.', 'app-factory' ),
                        'default' => ''
                    ),
                    'author_url' => array(
                        'title' => 'Author URL',
                        'description' => __( 'Url of the lead developer on the app.', 'app-factory' ),
                        'default' => ''
                    ),
                    'ios_identifier' => array(
                        'title' => 'iOS Identifier',
                        'description' => __( 'Identification number of the application, for example: id345038631 ( note the "id" prefix ). If defined - used to redirect user to the applications page on iTunes if there is a new version required from a Hot Code Push', 'app-factory' ),
                        'default' => ''
                    )
                )
            );
        }
    }


    /**
     * When the post is saved, saves our custom data.
     *
     * @param int $post_id - the post id for the app post being saved
     *
     * @return void.
     */
    public function save_postdata( $post_id )
    {
        // verify if this is an auto save routine.
        // If it is our form has not been submitted, so we dont want to do anything
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        // verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times
        if ( !wp_verify_nonce( $_POST['app_meta_nonce'], 'wp-app-factory' ) ) {
            return;
        }

        $post = get_post($post_id);

        //skip all cases where we shouldn't index
        if ( $post->post_type != APP_POST_TYPE ) {
            return;
        }

        // Just in case a setting isn't outputted to the admin page ( like 'private' variables ), then make sure they
        // persist
        $settings = self::getSettings( $post_id );
        foreach ( $settings as $key => $value ) {
            if ( !isset( $_POST['app_meta'][$key] ) ) {
                $_POST['app_meta'][$key] = $value;
            }
        }

        if ( !empty( $_POST['app_meta']['bundle_id'] ) ) {
            $_POST['app_meta']['bundle_id'] = trim( $_POST['app_meta']['bundle_id'], '.' );
        }

        self::saveSettings( $post_id, $_POST['app_meta'] );
    }

    /**
     * Retrieves the settings for the provided post id.  If omitted, it simply returns the default settings.
     *
     * @param null|integer $post_id the post id
     *
     * @return array of settings where the key is the setting slug and the value is the value
     */
    public static function getSettings( $post_id = null )
    {
        self::standup();

        if ( isset( $post_id ) ) {
            $app_meta = get_post_meta( $post_id, 'app_meta', true );
        }
        if ( empty( $app_meta ) ) {
            $app_meta = array();
        }

        // Make sure $app_meta doesn't have any settings that aren't allowed
        foreach ( array_keys( $app_meta ) as $slug ) {
            if ( !isset( self::$settings[ $slug ] ) ) {
                unset( $app_meta[$slug] );
            }
        }

        // Make sure that $app_meta has a setting for all settings that are allowed
        foreach ( self::$settings as $slug => $setting ) {
            if ( empty( $app_meta[$slug] ) ) {
                $app_meta[$slug] = $setting['default'];
            }
        }

        return $app_meta;
    }

    /**
     * Save the settings to the database.
     *
     * @param integer    $post_id  the post id
     * @param null|array $settings the array of settings
     *
     * @return void
     */
    public static function saveSettings( $post_id, $settings = null )
    {
        if ( isset( $settings ) ) {
            if ( !empty( $settings['ios_identifier'] ) && substr( $settings['ios_identifier'], 0, 2 ) !== 'id' ) {
                $settings['ios_identifier'] = 'id' . $settings['ios_identifier'];
            }
            update_post_meta( $post_id, 'app_meta', $settings );
        } else {
            delete_post_meta( $post_id,'app_meta' );
        }
    }

    /**
     * Markup for the meta box on the app page that pertains to the settings
     *
     * @param object $app the current post
     *
     * @return void
     */
    public static function settings_metabox( $app )
    {
        $the_app = & TheAppFactory::instantiateFromPost( $app, 'TheAppSenchaPackager' );

        self::standup();

        $app_meta = self::getSettings( $app->ID );
        $defaults = $the_app->get_default_atts( 'the_app' );

        wp_nonce_field( 'wp-app-factory', 'app_meta_nonce' );
        ?>

        <p><?php _e( 'These settings define necessary variables used when packaging the app.', 'app-factory' ); ?></p>
        <?php foreach ( self::$settings as $slug => $setting ) : ?>
            <div style="margin:20px 0">
                <label style="display:inline-block;width:160px;vertical-align:top"><?php echo $setting['title']; ?>:</label>
                <?php if ( isset( $setting['type'] ) && $setting['type'] != 'input' ) : ?>
                    <?php
                    switch ( $setting['type'] ) :
                    case 'textarea': ?>
                        <textarea name="app_meta[<?php echo $slug; ?>]" cols="50" rows="8"><?php echo esc_html( $app_meta[$slug] ); ?></textarea>
                        <?php break;
                    case 'private':
                        // Do not output it
                        break;
                    endswitch;
                    ?>
                <?php else : ?>
                    <input type="text" name="app_meta[<?php echo $slug; ?>]" value="<?php echo esc_attr( $app_meta[$slug] ); ?>"/>
                <?php endif; ?>
                <?php if ( 'bundle_id' == $slug ) {
                    echo ".$the_app->package_name"; // special case
                } ?>
                <p class="description"><?php echo $setting['description']; ?></p>
            </div>
        <?php endforeach;
    }
}